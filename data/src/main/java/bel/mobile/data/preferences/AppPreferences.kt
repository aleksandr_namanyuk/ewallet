package bel.mobile.data.preferences

import android.content.SharedPreferences
import javax.inject.Inject

class AppPreferences @Inject constructor(private val sharedPreferences: SharedPreferences){

    companion object {
        const val FIRST_LAUNCH_TOGGLE = "first_launch"
        const val APP_CURRENCY = "app_currency"
    }

    fun saveAppCurrency(currency: String){
        sharedPreferences.edit().putString(APP_CURRENCY, currency).apply()
    }

    fun getAppCurrency(): String =
            sharedPreferences.getString(APP_CURRENCY, "")

    fun isFirstAppLaunch(): Boolean =
            sharedPreferences.getBoolean(FIRST_LAUNCH_TOGGLE, true)

    fun appFirstLaunchDone(){
        sharedPreferences.edit().putBoolean(FIRST_LAUNCH_TOGGLE, false).apply()
    }

}