package bel.mobile.data.source.local

import bel.mobile.data.model.Account
import io.objectbox.Box
import io.objectbox.BoxStore
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

class AccountDaoImpl (boxStore: BoxStore): AccountDao {

    private val accountBox: Box<Account> = boxStore.boxFor(Account::class.java)

    override fun put(entity: Account): Single<Long>  =
            Single.fromCallable { accountBox.put(entity) }

    override fun entity(entityId: Long): Observable<Account> =
            Observable.fromCallable{ accountBox.get(entityId) }

    override fun entities(): Single<MutableList<Account>> =
            Single.fromCallable { accountBox.query().build().find() }

    override fun deleteById(entityId: Long): Completable =
            Completable.fromCallable{ accountBox.remove(entityId) }!!
}