package bel.mobile.data.source.repository

import bel.mobile.data.source.local.CategoryDao
import bel.mobile.data.model.Category
import bel.mobile.data.source.repository.base.CategoryDataSource
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class CategoryRepository @Inject constructor(private val categoryDao: CategoryDao): CategoryDataSource {

    override fun put(item: Category): Single<Long> =
            categoryDao.put(item)

    override fun item(itemId: Long): Observable<Category> =
            categoryDao.entity(itemId)

    override fun items(): Single<MutableList<Category>> =
            categoryDao.entities()

    override fun categoriesByParentId(parentId: Long): Single<MutableList<Category>> =
            categoryDao.categoriesByParentId(parentId)

    override fun categoriesByType(type: Long): Single<MutableList<Category>> =
            categoryDao.categoriesByType(type)

    override fun deleteById(itemId: Long): Completable =
            categoryDao.deleteById(itemId)

    override fun populateTableWithDefaultData(): Completable {
        return categoryDao.populateTableWithDefaultData()
    }
}