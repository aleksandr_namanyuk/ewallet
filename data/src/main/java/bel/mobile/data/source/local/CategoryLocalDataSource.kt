package bel.mobile.data.source.local

import android.util.Log
import bel.mobile.data.db.customtypes.TransactionType
import bel.mobile.data.model.Category
import bel.mobile.data.model.Category_
import io.objectbox.Box
import io.objectbox.BoxStore
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Singleton

@Singleton
class CategoryLocalDataSource constructor(boxStore: BoxStore): CategoryDao {

    companion object {
        val DEF_ORDER = Category_.title
        val TAG = "CategoryLocalDataSource"
    }

//    TODO: check what is the lifecycle of Box instance
    private val box: Box<Category> = boxStore.boxFor(Category::class.java)

    override fun put(entity: Category): Single<Long> =
            Single.fromCallable { box.put(entity) }

    override fun entity(entityId: Long): Observable<Category> =
            Observable.fromCallable{ box.get(entityId) }

    override fun entities(): Single<MutableList<Category>> =
            Single.fromCallable {
                box.query()
                    .order(DEF_ORDER)
                    .build()
                    .find()
            }

    override fun deleteById(entityId: Long): Completable =
            Completable.fromCallable{ box.remove(entityId) }!!

    override fun categoriesByParentId(parentId: Long): Single<MutableList<Category>> =
            Single.fromCallable {
                box.query().equal(Category_.parentId, parentId)
                    .order(DEF_ORDER)
                    .build()
                    .find()
            }

    override fun categoriesByType(type: Long): Single<MutableList<Category>> =
        Single.fromCallable {
            box.query().equal(Category_.type, type)
                    .orderDesc(DEF_ORDER)
                    .build()
                    .find()
        }

    override fun categoriesBy() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun populateTableWithDefaultData(): Completable {
        val category1 = Category(title = "Food & Drinks", type = TransactionType.EXPENSE)
        box.put(category1)

        val category2 = Category(title = "Shopping", type = TransactionType.EXPENSE)
        box.put(category2)

        val category3 = Category(title = "Housing", type = TransactionType.EXPENSE)
        box.put(category3)

        val category24 = Category(title = "Toll road", type = TransactionType.EXPENSE)

        val category4 = Category(title = "Transportation", type = TransactionType.EXPENSE)
        category4.subCategories.add(category24)
        box.put(category4)

        val category5 = Category(title = "Vehicle", type = TransactionType.EXPENSE)
        box.put(category5)

        val category6 = Category(title = "Life & Entertainment", type = TransactionType.EXPENSE)
        box.put(category6)

        val category7 = Category(title = "Communication", type = TransactionType.EXPENSE)
        box.put(category7)

        val category8 = Category(title = "Financial expenses", type = TransactionType.EXPENSE)
        box.put(category8)

        val category9 = Category(title = "Groceries", type = TransactionType.EXPENSE)
        category9.parent.target = category1
        box.put(category9)

        val category10 = Category(title = "Bar, cafe", type = TransactionType.EXPENSE)
        category10.parent.target = category1
        box.put(category10)

        val category11 = Category(title = "Restaurant, fast-food", type = TransactionType.EXPENSE)
        category11.parent.target = category1
        box.put(category11)

        val category12 = Category(title = "Cloths & shoes", type = TransactionType.EXPENSE)
        category12.parent.target = category2
        box.put(category12)

        Log.d(TAG, "category1.getId : ${category2.id}")
        Log.d(TAG, "category9.parent.getTargetId : ${category12.parent.targetId} ")
        Log.d(TAG, "category9.parent.getTarget().getId : ${category12.parent.targetId} ")

        val category14 = Category(title = "For home", type = TransactionType.EXPENSE)
        category14.parent.target = category2
        box.put(category14)

        val category15 = Category(title = "Rent", type = TransactionType.EXPENSE)
        category15.parent.target = category3
        box.put(category15)

        val category16 = Category(title = "Electricity", type = TransactionType.EXPENSE)
        category16.parent.target = category3
        box.put(category16)

        val category17 = Category(title = "Water", type = TransactionType.EXPENSE)
        category17.parent.target = category3
        box.put(category17)

        val category18 = Category(title = "Public transport", type = TransactionType.EXPENSE)
//        category18.parent?setTarget(category4)
        box.put(category18)

        /**
         * Correct way ToOne - ToMany
         * */
        val category19 = Category(title = "Taxi", type = TransactionType.EXPENSE)
        category19.parent.target = category4
        box.put(category19)

        val category23 = Category(title = "Air plain", type = TransactionType.EXPENSE)
        category23.parent.target = category4

        category4.subCategories.add(category18)
        category4.subCategories.add(category19)
        category4.subCategories.add(category23)

        val category20 = Category(title = "Salary", type = TransactionType.INCOME)
        box.put(category20)

        val category21 = Category(title = "Sale", type = TransactionType.INCOME)
//        category21.setType(TYPE_INCOME)
        box.put(category21)

        val category22 = Category(title = "Others", type = TransactionType.INCOME)
//        category22.setType(TYPE_INCOME)
        box.put(category22)

        return Completable.complete()
    }
}