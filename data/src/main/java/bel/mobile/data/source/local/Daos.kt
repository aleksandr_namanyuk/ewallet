package bel.mobile.data.source.local

import bel.mobile.data.model.Account
import bel.mobile.data.model.Category
import bel.mobile.data.model.Record
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Interfaces represent each entity-table in the database from where local data is retrieved.
 * These classes are responsible for handling SQL commands from the database and
 * for providing them as a part of data flow.
 */

interface AccountDao: BaseDao<Account>

interface CategoryDao: BaseDao<Category> {
    fun populateTableWithDefaultData(): Completable
    fun categoriesByParentId(parentId: Long): Single<MutableList<Category>>
    fun categoriesByType(type: Long): Single<MutableList<Category>>
    fun categoriesBy()
}

interface RecordDao: BaseDao<Record>

interface BaseDao<T>{

    /**
     * Get an {@link Observable} which will emit an id of the inserted or updated T.
     *
     * @param entity The account used to be inserted or updated if already exist in db.
     */
    fun put(entity: T): Single<Long>

    /**
     * Get an {@link Observable} which will emit a T.
     *
     * @param entityId The account id used to retrieve account data.
     */
    fun entity(entityId: Long): Observable<T>

    /**
     * Get an {@link Observable} which will emit a List of T.
     */
    fun entities(): Single<MutableList<T>>

    /**
     * Get an {@link Observable} which will emit nothing.
     *
     * @param entityId The id used to delete T object.
     */
    fun deleteById(entityId: Long): Completable
}
