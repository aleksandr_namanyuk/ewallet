package bel.mobile.data.source.repository

import bel.mobile.data.source.local.AccountDao
import bel.mobile.data.model.Account
import bel.mobile.data.source.repository.base.AccountDataSource
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Repository for Account entity from all sources.
 * ! NOTE ! Now only DB source is implemented -> Local DAO.
 * Ideally it will be also RemoteDataSource
 */
class AccountRepository @Inject constructor(private val accountDao: AccountDao): AccountDataSource {

    override fun put(item: Account): Single<Long> =
            accountDao.put(item)

    override fun item(itemId: Long): Observable<Account> =
            accountDao.entity(itemId)

    override fun items(): Single<MutableList<Account>> =
            accountDao.entities()

    override fun deleteById(itemId: Long): Completable =
            accountDao.deleteById(itemId)
}