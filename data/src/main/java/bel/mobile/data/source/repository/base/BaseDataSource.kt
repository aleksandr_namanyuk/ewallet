package bel.mobile.data.source.repository.base

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Interface that represents a Repository for getting {@link T} related data.
 * Repository is working with database, cache, remote or other source of data.
 */
interface BaseDataSource<T>{

    /**
     * Get an {@link Observable} which will emit an id of the inserted or updated {@link AccountModel}.
     *
     * @param item The account used to be inserted or updated if already exist in db.
     */
    fun put(item: T): Single<Long>

    /**
     * Get an {@link Observable} which will emit a {@link AccountModel}.
     *
     * @param itemId The account id used to retrieve account data.
     */
    fun item(itemId: Long): Observable<T>

    /**
     * Get an {@link Observable} which will emit a List of {@link AccountModel}.
     */
    fun items(): Single<MutableList<T>>

    /**
     * Get an {@link Observable} which will emit nothing.
     *
     * @param itemId The account id used to delete an {@link AccountModel}.
     */
    fun deleteById(itemId: Long): Completable
}