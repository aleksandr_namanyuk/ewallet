package bel.mobile.data.source.repository.base

import bel.mobile.data.model.Account

/**
 * Interface that represents a Repository for getting {@link AccountModel} related data.
 * Repository is working with database, cache, remote or other source of data.
 */
interface AccountDataSource : BaseDataSource<Account>