package bel.mobile.data.source.repository.base

import bel.mobile.data.model.Category
import io.reactivex.Completable
import io.reactivex.Single

interface CategoryDataSource: BaseDataSource<Category> {

    fun populateTableWithDefaultData(): Completable
    fun categoriesByParentId(parentId: Long): Single<MutableList<Category>>
    fun categoriesByType(type: Long): Single<MutableList<Category>>
}