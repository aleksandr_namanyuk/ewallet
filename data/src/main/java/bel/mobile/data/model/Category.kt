package bel.mobile.data.model

import bel.mobile.data.db.CategoryTable
import bel.mobile.data.db.customtypes.TransactionType
import bel.mobile.data.db.customtypes.TransactionTypeConverter
import io.objectbox.BoxStore
import io.objectbox.annotation.Backlink
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToMany
import io.objectbox.relation.ToOne

/**
 * Category represent a way how you spend your money.
 */
// TODO : Category with id 1 should be default "WithoutCategory" item
@Entity
data class Category @JvmOverloads constructor(
        @Id var id: Long = 0,
//        @Convert(converter = DateTimeTypeConverter::class, dbType = Long::class)
//        var date: DateTime,
        var title: String = "",
        @Convert(converter = TransactionTypeConverter::class, dbType = Long::class)
        var type: TransactionType = TransactionType.EXPENSE,
        var parentId: Long = CategoryTable.defaultCategoryId
){
    // workaround for ObjectBox local unit tests
    @JvmField @Transient var __boxStore: BoxStore? = null

    @JvmField var parent: ToOne<Category> = ToOne(this, Category_.parent)

    @JvmField @Backlink(to = "parent")
    var subCategories: ToMany<Category> = ToMany<Category>(this, Category_.subCategories)

    @JvmField @Backlink(to = "category")
    var records: ToMany<Record> = ToMany<Record>(this, Category_.records)

    init {
        parent.targetId = parentId
    }

}