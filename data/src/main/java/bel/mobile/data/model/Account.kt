package bel.mobile.data.model

import bel.mobile.data.db.AccountTable
import io.objectbox.BoxStore
import io.objectbox.annotation.Backlink
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToMany

/**
 * Account Entity that represent one item of wallet.
 * For example: Debit Card, Credit, Cash, Saving account in bank
 */
@Entity
data class Account @JvmOverloads constructor(
        @Id var id: Long = 0,
        var name: String = "",
        var balance: Double = AccountTable.defaultBalance,
        var currency: Int = 1
){
    @JvmField @Transient
    var __boxStore: BoxStore? = null

    @JvmField @Backlink
    var records: ToMany<Record> = ToMany(this, Account_.records)
}