package bel.mobile.data.model

import bel.mobile.data.db.*
import bel.mobile.data.db.customtypes.TransactionType
import bel.mobile.data.db.customtypes.TransactionTypeConverter
import io.objectbox.BoxStore
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne

@Entity
data class Record @JvmOverloads constructor(
        @Id var id: Long = 0,
//        TODO : add date column
        @Convert(converter = TransactionTypeConverter::class, dbType = Integer::class)
        var type: TransactionType = TransactionType.EXPENSE,
        var amount: Double = RecordTable.defaultAmount,
        var description: String = "",
        var accountId: Long = AccountTable.defaultAccountId,
        var categoryId: Long = CategoryTable.defaultCategoryId
){
    // necessary for ObjectBox local unit tests
    @JvmField @Transient var __boxStore: BoxStore? = null

    @JvmField var account: ToOne<Account> = ToOne(this, Record_.account)
    @JvmField var category: ToOne<Category> = ToOne(this, Record_.category)

    init {
        account.targetId = accountId
        category.targetId = categoryId
    }

}