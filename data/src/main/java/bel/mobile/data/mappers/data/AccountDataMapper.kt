//package bel.mobile.data.mappers.data
//
//import bel.mobile.data.mappers.BaseMapper
//import bel.mobile.data.model.Account
//import bel.mobile.domain.model.AccountModel
//import java.util.*
//
//
///**
// * Mapper class used to transform {@link AccountModel} (in the presentation layer) to {@link Account} in the
// * data layer.
// */
//class AccountDataMapper: BaseMapper<AccountModel, Account> {
//
//    /**
//     * Transform a {@link AccountModel} into an {@link Account}.
//     *
//     * @param from Object to be transformed.
//     * @return {@link Account} if valid {@link AccountModel}
//     */
//    override fun transform(from: AccountModel): Account {
//        val account = Account()
//        account.id = from.id
//        account.name = from.title
//        account.balance = from.balance
//        account.currency = from.currency
////        from.records.forEach { account.records. }
//        return account
//    }
//
//    /**
//     * Transform a List of {@link AccountModel} into a Collection of {@link Account}.
//     *
//     * @param fromList Object Collection to be transformed.
//     * @return {@link Account} if valid {@link AccountModel} otherwise null.
//     */
//    override fun transform(fromList: MutableList<AccountModel>): MutableList<Account> {
//        val accountList = ArrayList<Account>(fromList.size)
//        return fromList.mapTo(accountList) { transform(it) }
//    }
//
//}