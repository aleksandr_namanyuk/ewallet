//package bel.mobile.data.mappers.data
//
//import bel.mobile.data.db.customtypes.TransactionType
//import bel.mobile.data.mappers.BaseMapper
//import bel.mobile.data.model.Record
//import bel.mobile.domain.model.RecordModel
//
//class RecordDataMapper: BaseMapper<RecordModel, Record> {
//
//    override fun transform(from: RecordModel): Record {
//        val record = Record()
//        record.id = from.id
//        record.type = TransactionType.Companion.convertType(from.type)
//        record.amount = from.amount
//        record.description = from.description
//        record.accountId = from.accountId
//        record.categoryId = from.categoryId
//        return record
//    }
//
//    override fun transform(fromList: MutableList<RecordModel>): MutableList<Record> {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//}