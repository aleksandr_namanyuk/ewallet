package bel.mobile.data.mappers

interface BaseMapper<DomainModel, Model>{

    fun transform(from: DomainModel): Model
    fun transform(fromList: MutableList<DomainModel>): MutableList<Model>
}