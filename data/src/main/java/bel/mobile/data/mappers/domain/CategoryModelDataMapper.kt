//package bel.mobile.data.mappers.domain
//
//import bel.mobile.data.mappers.BaseMapper
//import bel.mobile.data.model.Category
//import bel.mobile.domain.model.CategoryModel
//import java.util.ArrayList
//
//class CategoryModelDataMapper: BaseMapper<Category, CategoryModel>{
//
//    override fun transform(from: Category): CategoryModel {
//        val toModel = CategoryModel()
//        toModel.id = from.id
//        toModel.title = from.title
//        toModel.type = from.type.type()
//
////        toModel.parent = transform(from.parent.target)
////        from.subCategories.let {
////            if(it.size > 0) it.forEach { toModel.subCategoriesId.add(it.id) }
////        }
//
//        return toModel
//    }
//
//    override fun transform(fromList: MutableList<Category>): MutableList<CategoryModel> {
//        val toList = ArrayList<CategoryModel>(fromList.size)
//        return fromList.mapTo(toList) { transform(it) }
//    }
//}