//package bel.mobile.data.mappers.domain
//
//import bel.mobile.data.mappers.BaseMapper
//import bel.mobile.data.model.Account
//import bel.mobile.domain.model.AccountModel
//
///**
// * Mapper class used to transform {@link Account} (in the domain layer) to {@link AccountModel} in the
// * presentation layer.
// */
//class AccountModelDataMapper: BaseMapper<Account, AccountModel>{
//
//    /**
//     * Transform a {@link Account} into an {@link AccountModel}.
//     *
//     * @param from Object to be transformed.
//     * @return {@link AccountModel}.
//     */
//    override fun transform(from: Account): AccountModel {
//        val accountModel = AccountModel(from.id)
//        accountModel.title = from.name
//        accountModel.balance = from.balance
//        accountModel.currency = from.currency
//        from.records.forEach { accountModel.records.add(it.id) }
//        return accountModel
//    }
//
//    override fun transform(fromList: MutableList<Account>): MutableList<AccountModel> {
//        val accountModelList = ArrayList<AccountModel>(fromList.size)
//        return fromList.mapTo(accountModelList){ transform(it) }
//    }
//
//}