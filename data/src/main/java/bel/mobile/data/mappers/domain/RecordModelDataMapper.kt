//package bel.mobile.data.mappers.domain
//
//import bel.mobile.data.mappers.BaseMapper
//import bel.mobile.data.model.Record
//import bel.mobile.domain.model.RecordModel
//
//class RecordModelDataMapper: BaseMapper<Record, RecordModel>{
//
//    override fun transform(from: Record): RecordModel {
//        val to = RecordModel()
//        to.id = from.id
//        to.type = from.type.type()
//        to.amount = from.amount
//        to.description = from.description
//        to.accountId = from.account.targetId
//        to.categoryId = from.category.targetId
//        return to
//    }
//
//    override fun transform(fromList: MutableList<Record>): MutableList<RecordModel> {
//        val toList = ArrayList<RecordModel>(fromList.size)
//        return fromList.mapTo(toList) { transform(it) }
//    }
//}