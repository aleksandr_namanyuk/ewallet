package bel.mobile.data.db.interactor.base.core

import bel.mobile.data.db.interactor.base.CompletableInteractorWithParameter
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

abstract class CompletableUseCaseWithParameter<in Param> (private val threadExecutor: ThreadExecutor,
                                                          private val postExecutionThread: PostExecutionThread):
        UseCase(), CompletableInteractorWithParameter<Param> {

    abstract fun createCompletable(param: Param): Completable

    override fun execute(param: Param, completableObserver: DisposableCompletableObserver) {
        compositeDisposable.add(createCompletable(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(completableObserver))
    }

    override fun execute(param: Param): Completable =
            createCompletable(param)
}