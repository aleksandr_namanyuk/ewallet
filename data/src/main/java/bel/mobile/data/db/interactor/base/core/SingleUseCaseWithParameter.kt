package bel.mobile.data.db.interactor.base.core

import bel.mobile.data.db.interactor.base.SingleInteractorWithParameter
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCaseWithParameter<in Param, R> (private val threadExecutor: ThreadExecutor,
                                                        private val postExecutionThread: PostExecutionThread):
        UseCase(), SingleInteractorWithParameter<Param, R> {

    abstract fun createSingle(param: Param): Single<R>

    override fun execute(param: Param, singleObserver: DisposableSingleObserver<R>) {
        compositeDisposable.add(createSingle(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(singleObserver))
    }

    override fun execute(param: Param): Single<R> =
            createSingle(param)

}