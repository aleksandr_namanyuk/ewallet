package bel.mobile.data.db.interactor.base

import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver

interface ObservableInteractorWithParameter<in Param, R>{

    fun execute(param: Param, observer: DisposableObserver<R>)
    fun execute(param: Param): Observable<R>
}