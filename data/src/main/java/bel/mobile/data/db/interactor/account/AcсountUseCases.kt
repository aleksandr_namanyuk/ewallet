package bel.mobile.data.db.interactor.account

import bel.mobile.data.db.interactor.base.core.CompletableUseCaseWithParameter
import bel.mobile.data.db.interactor.base.core.ObservableUseCaseWithParameter
import bel.mobile.data.db.interactor.base.core.SingleUseCaseWithParameter
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.model.Account
import bel.mobile.data.source.repository.base.AccountDataSource
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Use cases for handling @{link Account}.
 */
class PutAccount @Inject constructor(private val accountRepository: AccountDataSource,
                                     threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread):
        ObservableUseCaseWithParameter<Account, Long>(threadExecutor, postExecutionThread) {

    override fun createObservable(param: Account): Observable<Long> {
        return accountRepository.put(param)
                .toObservable()
    }
}

class GetAccount @Inject constructor(private val accountRepository: AccountDataSource,
                                     threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread):
        ObservableUseCaseWithParameter<Long, Account>(threadExecutor, postExecutionThread) {

    override fun createObservable(param: Long): Observable<Account> {
        return accountRepository.item(param)
    }
}

class DeleteAccount @Inject constructor(private val accountRepository: AccountDataSource,
                                        threadExecutor: ThreadExecutor,
                                        postExecutionThread: PostExecutionThread):
        CompletableUseCaseWithParameter<Long>(threadExecutor, postExecutionThread) {

    override fun createCompletable(param: Long): Completable =
        accountRepository.deleteById(param)
}