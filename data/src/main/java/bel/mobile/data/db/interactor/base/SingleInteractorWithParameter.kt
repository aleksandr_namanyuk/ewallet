package bel.mobile.data.db.interactor.base

import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

interface SingleInteractorWithParameter<in Param, R>{

    fun execute(param: Param, singleObserver: DisposableSingleObserver<R>)
    fun execute(param: Param): Single<R>
}