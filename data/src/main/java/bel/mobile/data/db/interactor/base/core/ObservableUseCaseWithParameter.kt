package bel.mobile.data.db.interactor.base.core

import bel.mobile.data.db.interactor.base.ObservableInteractorWithParameter
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

/**
 * Abstract UseCase for specified Data <T>, that takes parameter <Param> as input.
 */
abstract class ObservableUseCaseWithParameter<in Param, R>(private val threadExecutor: ThreadExecutor,
                                                           private val postExecutionThread: PostExecutionThread):
        UseCase(), ObservableInteractorWithParameter<Param, R> {

    abstract fun createObservable(param: Param): Observable<R>

    override fun execute(param: Param, observer: DisposableObserver<R>) {
        compositeDisposable.add(createObservable(param)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(observer))
    }

    override fun execute(param: Param): Observable<R> =
            createObservable(param)
}