package bel.mobile.data.db.interactor.categories

import bel.mobile.data.db.interactor.base.core.SingleUseCaseWithParameter
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.model.Category
import bel.mobile.data.source.repository.base.CategoryDataSource
import io.reactivex.Single

class GetCategoryListByType(private val categoryRepository: CategoryDataSource,
                            threadExecutor: ThreadExecutor,
                            postExecutionThread: PostExecutionThread):
        SingleUseCaseWithParameter<Long, MutableList<Category>>(threadExecutor, postExecutionThread){

    override fun createSingle(param: Long): Single<MutableList<Category>> {
        return categoryRepository.categoriesByType(param)
    }
}