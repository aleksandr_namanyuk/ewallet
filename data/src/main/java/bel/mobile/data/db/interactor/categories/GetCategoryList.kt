package bel.mobile.data.db.interactor.categories

import bel.mobile.data.db.interactor.base.core.SingleUseCase
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.model.Category
import bel.mobile.data.source.repository.base.CategoryDataSource
import io.reactivex.Single

class GetCategoryList(private val categoryRepository: CategoryDataSource,
                      threadExecutor: ThreadExecutor,
                      postExecutionThread: PostExecutionThread):
        SingleUseCase<MutableList<Category>>(threadExecutor, postExecutionThread){

    override fun createSingle(): Single<MutableList<Category>> {
        return categoryRepository.items()
    }
}