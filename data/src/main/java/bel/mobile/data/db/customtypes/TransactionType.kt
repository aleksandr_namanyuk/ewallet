package bel.mobile.data.db.customtypes

import io.objectbox.converter.PropertyConverter

sealed class TransactionType{
    object INCOME: TransactionType(){
        override fun type(): Long = TYPE_INCOME
    }
    object EXPENSE: TransactionType(){
        override fun type(): Long = TYPE_EXPENSE
    }
    object TRANSFER: TransactionType(){
        override fun type(): Long = TYPE_TRANSFER
    }

    abstract fun type(): Long

    companion object {
        private const val TYPE_INCOME = 0L
        private const val TYPE_EXPENSE = 1L
        private const val TYPE_TRANSFER = 2L

        fun convertType(typeId: Long): TransactionType =
                when(typeId){
                    TYPE_INCOME -> INCOME
                    TYPE_EXPENSE -> EXPENSE
                    TYPE_TRANSFER -> TRANSFER
                    else -> throw IllegalArgumentException("Illegal argument type.")
                }

        fun convertType(type: TransactionType): Long =
                when(type){
                    INCOME -> TYPE_INCOME
                    EXPENSE -> TYPE_EXPENSE
                    TRANSFER -> TYPE_TRANSFER
                }
    }
}

class TransactionTypeConverter: PropertyConverter<TransactionType, Long> {

    override fun convertToDatabaseValue(entityProperty: TransactionType): Long =
                entityProperty.type()

     override fun convertToEntityProperty(databaseValue: Long): TransactionType =
             TransactionType.convertType(databaseValue)
}