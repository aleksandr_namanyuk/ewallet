package bel.mobile.data.db.interactor.base.core

import bel.mobile.data.db.interactor.base.CompletableInteractor
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import io.reactivex.Completable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.schedulers.Schedulers

abstract class CompletableUseCase (private val threadExecutor: ThreadExecutor,
                                      private val postExecutionThread: PostExecutionThread):
        UseCase(), CompletableInteractor {

    abstract fun createCompletable(): Completable

    override fun execute(completable: DisposableCompletableObserver) {
        compositeDisposable.add(createCompletable()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(completable))
    }

    override fun execute(): Completable =
            createCompletable()
}