package bel.mobile.data.db

/**
 * Tables in the database.
 */

object AccountTable {
    const val defaultBalance = 0.0
    const val defaultCurrency = 1
    const val defaultAccountId: Long = 0
}

object RecordTable {
    const val defaultAmount = 0.0
}

object CategoryTable {
    const val defaultCategoryId: Long = 0
}