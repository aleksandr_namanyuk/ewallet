package bel.mobile.data.db.interactor.categories

import bel.mobile.data.db.interactor.base.core.CompletableUseCase
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.source.repository.base.CategoryDataSource
import io.reactivex.Completable

class PrepopulateCategories(private val categoryRepository: CategoryDataSource,
                            threadExecutor: ThreadExecutor,
                            postExecutionThread: PostExecutionThread):
        CompletableUseCase(threadExecutor, postExecutionThread){

    override fun createCompletable(): Completable {
        return categoryRepository.populateTableWithDefaultData()
    }

}