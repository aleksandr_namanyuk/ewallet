package bel.mobile.data.db.interactor.base.core

import io.reactivex.disposables.CompositeDisposable

/**
 * Main abstract class in the Domain module.
 *
 * Use case is responsible for handling validation, reporting and other pre-processing tasks.
 * Should be extended for each separate component to handle specific task achieving
 * Single Responsibility principle.
 */
// TODO : decide is it better to control CompositeDisposables for Usecases in the ViewModel ?
abstract class UseCase{

    protected val compositeDisposable = CompositeDisposable()

    fun dispose() = compositeDisposable.clear()
}