package bel.mobile.data.db.interactor.accounts

import bel.mobile.data.db.interactor.base.core.SingleUseCase
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.model.Account
import bel.mobile.data.source.repository.base.AccountDataSource
import io.reactivex.Single
import javax.inject.Inject


class GetAccountsList @Inject constructor(private val accountRepository: AccountDataSource,
                                          threadExecutor: ThreadExecutor,
                                          postExecutionThread: PostExecutionThread):
        SingleUseCase<MutableList<Account>>(threadExecutor, postExecutionThread){

    override fun createSingle(): Single<MutableList<Account>> {
        return accountRepository.items()
    }
}