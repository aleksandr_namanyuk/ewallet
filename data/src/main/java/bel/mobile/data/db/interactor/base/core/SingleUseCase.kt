package bel.mobile.data.db.interactor.base.core

import bel.mobile.data.db.interactor.base.SingleInteractor
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCase<R> (private val threadExecutor: ThreadExecutor,
                                 private val postExecutionThread: PostExecutionThread):
        UseCase(), SingleInteractor<R> {

    abstract fun createSingle(): Single<R>

    override fun execute(singleObserver: DisposableSingleObserver<R>) {
        compositeDisposable.add(createSingle()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler)
                .subscribeWith(singleObserver))
    }

    override fun execute(): Single<R> =
            createSingle()

}