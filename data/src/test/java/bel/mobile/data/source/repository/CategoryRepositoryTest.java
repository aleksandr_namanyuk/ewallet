package bel.mobile.data.source.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.source.local.CategoryDao;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.mappers.data.CategoryDataMapper;
import bel.mobile.data.mappers.domain.CategoryModelDataMapper;
import bel.mobile.data.model.Category;
import bel.mobile.domain.model.CategoryModel;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static bel.mobile.data.DataTestData.TEST_CATEGORY;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_ID_1;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_MODEL;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class CategoryRepositoryTest {
    
    private CategoryRepository categoryRepository;
    
    @Mock private CategoryDao mockCategoryDao;
    @Mock private CategoryDataMapper mockCategoryDataMapper;
    @Mock private CategoryModelDataMapper mockCategoryModelDataMapper;
    
    @Before
    public void setUp(){
        categoryRepository = new CategoryRepository(mockCategoryDao,
                mockCategoryDataMapper, mockCategoryModelDataMapper);
    }

    @Test
    public void testPutCategory(){
        when(mockCategoryDao.put(TEST_CATEGORY)).thenReturn(Single.just(TEST_CATEGORY_ID_1));
        when(mockCategoryDataMapper.transform(TEST_CATEGORY_MODEL)).thenReturn(TEST_CATEGORY);

        final TestObserver<Long> testSubscriber = new TestObserver<>();
        categoryRepository.put(TEST_CATEGORY_MODEL).subscribe(testSubscriber);

        testSubscriber.assertSubscribed();
        testSubscriber.assertValue(TEST_CATEGORY_ID_1);

        verify(mockCategoryDao).put(TEST_CATEGORY);
        verifyNoMoreInteractions(mockCategoryDao);
    }

    @Test
    public void testGetCategory(){
        doReturn(Observable.just(TEST_CATEGORY)).when(mockCategoryDao).entity(TEST_CATEGORY_ID_1);
        when(mockCategoryModelDataMapper.transform(TEST_CATEGORY)).thenReturn(TEST_CATEGORY_MODEL);

        final TestObserver<CategoryModel> testSubscriber = new TestObserver<>();
        categoryRepository.item(TEST_CATEGORY_ID_1).subscribe(testSubscriber);

        testSubscriber.assertSubscribed();
        testSubscriber.assertComplete();
        testSubscriber.assertValue(TEST_CATEGORY_MODEL);

        verify(mockCategoryDao).entity(TEST_CATEGORY_ID_1);
        verifyNoMoreInteractions(mockCategoryDao);
    }

    @Test
    public void testGetCategories(){
        List<Category> accountList = new ArrayList<>();
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);

        doReturn(Single.just(accountList)).when(mockCategoryDao).entities();

        final TestObserver<List<CategoryModel>> testObserver = new TestObserver<>();
        categoryRepository.items().subscribe(testObserver);

        verify(mockCategoryDao).entities();
        verifyNoMoreInteractions(mockCategoryDao);

        testObserver.assertComplete();
    }

    @Test
    public void testDeleteAccount(){
        doReturn(Completable.complete()).when(mockCategoryDao).deleteById(TEST_CATEGORY_ID_1);

        final TestObserver testObserver = new TestObserver();
        categoryRepository.deleteById(TEST_CATEGORY_ID_1).subscribe(testObserver);

        verify(mockCategoryDao).deleteById(TEST_CATEGORY_ID_1);

        testObserver.assertComplete();
    }

    @Test
    public void testGetCategoriesByParent(){
        List<Category> accountList = new ArrayList<>();
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);

        when(mockCategoryDao.categoriesByParentId(1)).thenReturn(Single.just(accountList));

        final TestObserver<List<CategoryModel>> testObserver = new TestObserver<>();
        categoryRepository.categoriesByParentId(1).subscribe(testObserver);

        verify(mockCategoryDao).categoriesByParentId(1);
        verifyNoMoreInteractions(mockCategoryDao);

        testObserver.assertComplete();
    }

    @Test
    public void testGetCategoriesByType(){
        List<Category> accountList = new ArrayList<>();
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);
        accountList.add(TEST_CATEGORY);

        when(mockCategoryDao.categoriesByType(TransactionType.EXPENSE.INSTANCE.type())).thenReturn(Single.just(accountList));

        final TestObserver<List<CategoryModel>> testObserver = new TestObserver<>();
        categoryRepository.categoriesByType(1).subscribe(testObserver);

        verify(mockCategoryDao).categoriesByType(1);
        verifyNoMoreInteractions(mockCategoryDao);

        testObserver.assertComplete();
    }

}
