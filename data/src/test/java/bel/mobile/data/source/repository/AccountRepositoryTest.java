package bel.mobile.data.source.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.source.local.AccountDao;
import bel.mobile.data.mappers.data.AccountDataMapper;
import bel.mobile.data.mappers.domain.AccountModelDataMapper;
import bel.mobile.data.model.Account;
import bel.mobile.domain.model.AccountModel;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static bel.mobile.data.DataTestData.TEST_ACCOUNT;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_ID_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_MODEL;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class AccountRepositoryTest {

    private AccountRepository accountRepository;

    @Mock private AccountDao mockAccountDao;
//    @Mock private AccountDataMapper mockAccountDataMapper;
//    @Mock private AccountModelDataMapper mockAccountModelDataMapper;

    @Before
    public void setUp(){
        accountRepository = new AccountRepository(mockAccountDao
                /*mockAccountDataMapper, mockAccountModelDataMapper*/);
    }

    @Test
    public void testPutNewAccountInDatabase(){
        when(mockAccountDao.put(TEST_ACCOUNT)).thenReturn(Single.just(TEST_ACCOUNT_ID_1));
//        when(mockAccountDataMapper.transform(TEST_ACCOUNT_MODEL)).thenReturn(TEST_ACCOUNT);

        final TestObserver<Long> testSubscriber = new TestObserver<>();
        accountRepository.put(TEST_ACCOUNT_MODEL).subscribe(testSubscriber);

        testSubscriber.assertSubscribed();
        testSubscriber.assertValue(TEST_ACCOUNT_ID_1);

        verify(mockAccountDao).put(TEST_ACCOUNT);
        verifyNoMoreInteractions(mockAccountDao);
    }

    @Test
    public void testGetAccount(){
        doReturn(Observable.just(TEST_ACCOUNT)).when(mockAccountDao).entity(TEST_ACCOUNT_ID_1);
        when(mockAccountModelDataMapper.transform(TEST_ACCOUNT)).thenReturn(TEST_ACCOUNT_MODEL);

        final TestObserver<AccountModel> testSubscriber = new TestObserver<>();
        accountRepository.item(TEST_ACCOUNT_ID_1).subscribe(testSubscriber);

        testSubscriber.assertSubscribed();
        testSubscriber.assertComplete();
        testSubscriber.assertValue(TEST_ACCOUNT_MODEL);

        verify(mockAccountDao).entity(TEST_ACCOUNT_ID_1);
        verifyNoMoreInteractions(mockAccountDao);
    }

    @Test
    public void testGetAccounts(){
        List<Account> accountList = new ArrayList<>();
        accountList.add(TEST_ACCOUNT);
        accountList.add(TEST_ACCOUNT);
        accountList.add(TEST_ACCOUNT);

        doReturn(Single.just(accountList)).when(mockAccountDao).entities();

        final TestObserver<List<AccountModel>> testObserver = new TestObserver<>();
        accountRepository.items().subscribe(testObserver);

        verify(mockAccountDao).entities();
        verifyNoMoreInteractions(mockAccountDao);

        testObserver.assertComplete();
    }

    @Test
    public void testDeleteAccount(){
        doReturn(Completable.complete()).when(mockAccountDao).deleteById(TEST_ACCOUNT_ID_1);

        final TestObserver testObserver = new TestObserver();
        accountRepository.deleteById(TEST_ACCOUNT_ID_1).subscribe(testObserver);

        verify(mockAccountDao).deleteById(TEST_ACCOUNT_ID_1);

        testObserver.assertComplete();
    }

}
