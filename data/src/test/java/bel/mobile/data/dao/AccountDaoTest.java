package bel.mobile.data.dao;

import org.junit.Test;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.source.local.AccountDaoImpl;
import bel.mobile.data.model.Account;
import io.reactivex.observers.TestObserver;

public class AccountDaoTest extends AbstractObjectBoxTest {

    private TestObserver<Account> testAccountSubscriber;
    private AccountDaoImpl accountDao;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        testAccountSubscriber = new TestObserver<>();
        accountDao = new AccountDaoImpl(getBox());
    }

    @Test
    public void testShouldPutSuccessfully(){
        final Account account = new Account();
        account.setName("Debit");

        final TestObserver<Long> testIdSubscriber = new TestObserver<>();
        accountDao.put(account).subscribe(testIdSubscriber);

        testIdSubscriber.assertValue(1L);
        testIdSubscriber.assertSubscribed();
    }

    @Test
    public void testShouldGetWithError(){
        accountDao.entity(1).subscribe(testAccountSubscriber);
        testAccountSubscriber.assertError(java.lang.NullPointerException.class);
    }

    @Test
    public void testShouldGetSuccessfully(){
        Account account = new Account();
        account.setName("Credit");
        account.setBalance(12.5);
        account.setCurrency(1);

        accountDao.put(account);
        accountDao.entity(1).subscribe(testAccountSubscriber);

        testAccountSubscriber.assertSubscribed();
    }


}
