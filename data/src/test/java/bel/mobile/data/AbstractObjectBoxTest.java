package bel.mobile.data;

import org.junit.After;
import org.junit.Before;

import java.io.File;

import bel.mobile.data.model.Account;
import bel.mobile.data.model.Category;
import bel.mobile.data.model.MyObjectBox;
import bel.mobile.data.model.Record;
import io.objectbox.Box;
import io.objectbox.BoxStore;
import io.objectbox.DebugFlags;

import static org.junit.Assert.assertNotNull;

public class AbstractObjectBoxTest {

    private static final File TEST_DIRECTORY = new File("data/test-db");

    protected BoxStore store;

    @Before
    public void setUp() throws Exception {
        // delete database files before each test to start with a clean database
        BoxStore.deleteAllFiles(TEST_DIRECTORY);
        store = MyObjectBox.builder()
                // add directory flag to change where ObjectBox puts its database files
                .directory(TEST_DIRECTORY)
                // optional: add debug flags for more detailed ObjectBox log output
                .debugFlags(DebugFlags.LOG_QUERIES | DebugFlags.LOG_QUERY_PARAMETERS)
                .build();
        assertNotNull(store);
    }

    @After
    public void tearDown() throws Exception {
        if (store != null) {
            store.close();
            store = null;
        }
        BoxStore.deleteAllFiles(TEST_DIRECTORY);
    }

    protected BoxStore getBox(){
        return store;
    }

    protected Box<Account> getAccountBox(){
        return store.boxFor(Account.class);
    }

    protected Box<Category> getCategoryBox(){
        return store.boxFor(Category.class);
    }

    protected Box<Record> getRecordBox(){
        return store.boxFor(Record.class);
    }

}
