package bel.mobile.data.mappers.domain;

import org.junit.Before;
import org.junit.Test;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.CategoryTable;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.model.Category;
import bel.mobile.domain.model.CategoryModel;
import io.objectbox.Box;

import static bel.mobile.data.DataTestData.TEST_CATEGORY_TITLE_1;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_TYPE_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CategoryModelDataMapperTest extends AbstractObjectBoxTest{

    private CategoryModelDataMapper categoryModelDataMapper = new CategoryModelDataMapper();
    private Box<Category> categoryBox;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        categoryModelDataMapper = new CategoryModelDataMapper();
        categoryBox = getCategoryBox();
    }

    @Test
    public void testShouldTransformDomainModelWithoutParentToEntityCorrectly() throws Exception{
        final Category category = new Category(0L, TEST_CATEGORY_TITLE_1,
                TEST_CATEGORY_TYPE_1, CategoryTable.defaultCategoryId);

        long id = categoryBox.put(category);

        final Category categoryFromBox = categoryBox.get(id);
        final CategoryModel categoryModel = categoryModelDataMapper.transform(categoryFromBox);

        assertEquals(id, categoryModel.getId());
        assertEquals(TEST_CATEGORY_TITLE_1, categoryModel.getTitle());
        assertEquals(TEST_CATEGORY_TYPE_1.type(), categoryModel.getType());
//        assertNull(categoryModel.getParent());
    }

    @Test
    public void testShouldTransformModelWithParentToDomainModelCorrectly() throws Exception{
        final Category withoutCategory = new Category(0L, "WithoutCategory",
                TransactionType.TRANSFER.INSTANCE, CategoryTable.defaultCategoryId);
        categoryBox.put(withoutCategory);

        final Category category = new Category(0L, "Food",
                TransactionType.TRANSFER.INSTANCE, CategoryTable.defaultCategoryId);
        final Category parent = new Category(0L, TEST_CATEGORY_TITLE_1,
                TEST_CATEGORY_TYPE_1, CategoryTable.defaultCategoryId);

        assertNotNull(category.parent);
        category.parent.setTarget(parent);

        long id = categoryBox.put(category);
        final Category categoryFromBox = categoryBox.get(id);

        final CategoryModel categoryModel = categoryModelDataMapper.transform(categoryFromBox);

        assertEquals(id, categoryModel.getId());
        assertEquals(category.getTitle(), categoryModel.getTitle());
        assertEquals(category.getType().type(), categoryModel.getType());

//        assertNotNull(categoryModel.getParent());
//
//        assertEquals(TEST_CATEGORY.getId(), categoryModel.getParent().getId());
//        assertEquals(TEST_CATEGORY_TITLE_1, categoryModel.getParent().getTitle());
//        assertEquals(TEST_CATEGORY_TYPE_1.type(), categoryModel.getParent().getType());

        assertNotNull(category.subCategories);
    }

    @Test
    public void testShouldTransformModelWithSubcategoriesToDomainModel() throws Exception{
        final Category parent = new Category(0L, TEST_CATEGORY_TITLE_1,
                TEST_CATEGORY_TYPE_1, CategoryTable.defaultCategoryId);
        final Category subCategory = new Category(0L, "Food",
                TransactionType.INCOME.INSTANCE, CategoryTable.defaultCategoryId);

        assertNotNull(subCategory.parent);
        assertNotNull(parent.subCategories);

        parent.subCategories.add(subCategory);

        long id = categoryBox.put(parent);
        final CategoryModel categoryModel = categoryModelDataMapper.transform(parent);

        assertEquals(id, categoryModel.getId());
        assertEquals(TEST_CATEGORY_TITLE_1, categoryModel.getTitle());
        assertEquals(TEST_CATEGORY_TYPE_1.type(), categoryModel.getType());

//        assertNotNull(categoryModel.getSubCategoriesId());
//        final List<Long> subCategoryIdList = categoryModel.getSubCategoriesId();
//
//        final long id1 = subCategoryIdList.get(0);
//        final Category subCategoryFromBox = categoryBox.get(id1);
//
//        assertEquals(subCategory.getTitle(), subCategoryFromBox.getTitle());
//        assertEquals(subCategory.getType().type(), subCategoryFromBox.getType().type());
    }

}
