package bel.mobile.data.mappers.data;

import org.junit.Before;
import org.junit.Test;

import bel.mobile.data.model.Category;
import bel.mobile.domain.model.CategoryModel;

import static bel.mobile.data.DataTestData.TEST_CATEGORY_ID_1;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_TITLE_1;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_TYPE_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class CategoryDataMapperTest {

    private CategoryDataMapper categoryDataMapper = new CategoryDataMapper();

    @Before
    public void setUp() throws Exception {
        categoryDataMapper = new CategoryDataMapper();
    }

    @Test
    public void testShouldTransformModelWithoutParentToDomainModelCorrectly() throws Exception{
        final CategoryModel accountModel = new CategoryModel(TEST_CATEGORY_ID_1, TEST_CATEGORY_TITLE_1,
                TEST_CATEGORY_TYPE_1.type());
        final Category category = categoryDataMapper.transform(accountModel);

        assertEquals(TEST_CATEGORY_ID_1, category.getId());
        assertEquals(TEST_CATEGORY_TITLE_1, category.getTitle());
        assertEquals(TEST_CATEGORY_TYPE_1.type(), category.getType().type());
        assertNotNull(category.parent);
//        assertNotNull(category.parent.getTarget());
    }

    @Test
    public void testShouldTransformModelWithParentToDomainModelCorrectly() throws Exception{
        final CategoryModel accountModel = new CategoryModel(TEST_CATEGORY_ID_1, TEST_CATEGORY_TITLE_1,
                TEST_CATEGORY_TYPE_1.type());
        final Category category = categoryDataMapper.transform(accountModel);

        assertEquals(TEST_CATEGORY_ID_1, category.getId());
        assertEquals(TEST_CATEGORY_TITLE_1, category.getTitle());
        assertEquals(TEST_CATEGORY_TYPE_1.type(), category.getType().type());

        assertNotNull(category.parent);
//        assertNotNull(category.parent.getTarget());
//        assertEquals(TEST_CATEGORY.getId(), category.parent.getTarget().getId());
//        assertEquals(TEST_CATEGORY.getTitle(), category.parent.getTarget().getTitle());
//        assertEquals(TEST_CATEGORY.getType().type(), category.parent.getTarget().getType().type());
        assertNotNull(category.subCategories);
        assertFalse(category.subCategories.isResolved());
    }

}
