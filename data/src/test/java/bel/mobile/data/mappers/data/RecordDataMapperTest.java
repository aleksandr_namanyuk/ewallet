package bel.mobile.data.mappers.data;

import org.junit.Before;
import org.junit.Test;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.CategoryTable;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.model.Account;
import bel.mobile.data.model.Category;
import bel.mobile.data.model.Record;
import bel.mobile.domain.model.RecordModel;
import io.objectbox.Box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class RecordDataMapperTest extends AbstractObjectBoxTest{

    private Box<Record> recordBox;
    private Box<Account> accountBox;
    private Box<Category> categoryBox;
    private RecordDataMapper recordDataMapper;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        recordDataMapper = new RecordDataMapper();
        recordBox = getRecordBox();
        accountBox = getAccountBox();
        categoryBox = getCategoryBox();
    }

    @Test
    public void testTransformDataModelToModel() throws Exception {
        Account account = new Account(0, "Debit", (double) 160, 1);
        Category category = new Category(0, "Food", TransactionType.EXPENSE.INSTANCE, CategoryTable.defaultCategoryId);

        long accountFromBoxId = accountBox.put(account);
        long categoryFromBoxId = categoryBox.put(category);

        RecordModel recordModel = new RecordModel(0, TransactionType.INCOME.INSTANCE.type(), 15.4,
                "Description", accountFromBoxId, categoryFromBoxId);

        Record recordTransformed = recordDataMapper.transform(recordModel);
        recordTransformed.account.setTarget(accountBox.get(recordTransformed.getAccountId()));
        recordTransformed.category.setTarget(categoryBox.get(recordModel.getCategoryId()));

        long id = recordBox.put(recordTransformed);

        Record transformedRecordFromBox = recordBox.get(id);

        assertEquals(transformedRecordFromBox.getId(), 1);
        assertEquals(transformedRecordFromBox.getType().type(), recordModel.getType());
        assertEquals(transformedRecordFromBox.getAmount(), recordModel.getAmount(), 0.0);
        assertEquals(transformedRecordFromBox.getDescription(), recordModel.getDescription());

        assertNotNull(transformedRecordFromBox.account);

        assertNotNull(transformedRecordFromBox.account.getTarget());
        assertEquals(account.getName(), transformedRecordFromBox.account.getTarget().getName());
        assertEquals(category.getTitle(), transformedRecordFromBox.category.getTarget().getTitle());

    }
}
