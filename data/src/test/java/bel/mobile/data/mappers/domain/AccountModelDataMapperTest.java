package bel.mobile.data.mappers.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.mappers.data.AccountDataMapper;
import bel.mobile.data.model.Account;
import bel.mobile.data.model.Record;
import bel.mobile.domain.model.AccountModel;
import io.objectbox.Box;

import static bel.mobile.data.DataTestData.TEST_ACCOUNT_BALANCE_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_BALANCE_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_CURRENCY_ID_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_CURRENCY_ID_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_ID_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_ID_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_TITLE_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_TITLE_2;
import static org.junit.Assert.assertEquals;

public class AccountModelDataMapperTest extends AbstractObjectBoxTest {

    private AccountDataMapper accountDataMapper;
    private AccountModelDataMapper accountModelDataMapper;
    private Box<Account> accountBox;
    private Box<Record> recordBox;

    @Before
    public void setUp() throws Exception{
        super.setUp();
        accountModelDataMapper = new AccountModelDataMapper();
        accountDataMapper = new AccountDataMapper();
        accountBox = getAccountBox();
        recordBox = getRecordBox();
    }

    /**
     * Test shows that after we insert (put) an Account to the database the item's ID changes automatically.
     * */
    @Test
    public void testShouldTransformEntityToModelSuccessfully() throws Exception{
        final Account account = new Account(0, TEST_ACCOUNT_TITLE_1,
                TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);

        assertEquals(0, account.getId());
        assertEquals(0, account.records.size());

        Record record1 = new Record(0, TransactionType.EXPENSE.INSTANCE, 12.9, "");
        account.records.add(record1);

        accountBox.put(account);

        final AccountModel accountModel = accountModelDataMapper.transform(account);

        // testing an actual Account item that was inserted into the database
        assertEquals(1, account.getId());
        assertEquals(1, account.records.size());

        // testing a mapped Account item used in the
        assertEquals(1, accountModel.getId());
        assertEquals(TEST_ACCOUNT_TITLE_1, accountModel.getTitle());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_1, accountModel.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_1, accountModel.getBalance(),0.0);
        assertEquals(1, accountModel.getRecords().size());

        final Account accountTransformed = accountDataMapper.transform(accountModel);

        assertEquals(1, accountTransformed.getId());
        assertEquals(TEST_ACCOUNT_TITLE_1, accountTransformed.getName());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_1, accountTransformed.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_1, accountTransformed.getBalance(), 0.0);
        assertEquals(12.9, accountTransformed.records.get(0).getAmount(), 0.0);
    }

    @Test
    public void testShouldTransformEntityListToModelListSuccessfully(){
        final Account account1 = new Account(TEST_ACCOUNT_ID_1, TEST_ACCOUNT_TITLE_1,
                TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);
        final Account account2 = new Account(TEST_ACCOUNT_ID_2, TEST_ACCOUNT_TITLE_2,
                TEST_ACCOUNT_BALANCE_2, TEST_ACCOUNT_CURRENCY_ID_2);

        final List<Account> accountList = new ArrayList<>();
        accountList.add(account1);
        accountList.add(account2);

        assertEquals(2, accountList.size());

        final List<AccountModel> accountModelList = accountModelDataMapper.transform(accountList);

        assertEquals(2 ,accountModelList.size());

        final AccountModel accountModel1 = accountModelList.get(0);
        final AccountModel accountModel2 = accountModelList.get(1);

        assertEquals(TEST_ACCOUNT_ID_1, accountModel1.getId());
        assertEquals(TEST_ACCOUNT_TITLE_1, accountModel1.getTitle());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_1, accountModel1.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_1, accountModel1.getBalance(), 0.0);

        assertEquals(TEST_ACCOUNT_ID_2, accountModel2.getId());
        assertEquals(TEST_ACCOUNT_TITLE_2, accountModel2.getTitle());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_2, accountModel2.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_2, accountModel2.getBalance(), 0.0);
    }

}
