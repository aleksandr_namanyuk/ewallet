package bel.mobile.data.mappers.data;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.mappers.data.AccountDataMapper;
import bel.mobile.data.model.Account;
import bel.mobile.domain.model.AccountModel;

import static bel.mobile.data.DataTestData.TEST_ACCOUNT_BALANCE_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_BALANCE_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_CURRENCY_ID_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_CURRENCY_ID_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_ID_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_ID_2;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_TITLE_1;
import static bel.mobile.data.DataTestData.TEST_ACCOUNT_TITLE_2;
import static org.junit.Assert.assertEquals;

public class AccountDataMapperTest {

    private AccountDataMapper accountDataMapper = new AccountDataMapper();

    @Before
    public void setUp() throws Exception {
        accountDataMapper = new AccountDataMapper();
    }

    @Test
    public void testShouldTransformModelToDomainModelCorrectly() throws Exception{
        final AccountModel accountModel = new AccountModel(TEST_ACCOUNT_ID_1, TEST_ACCOUNT_TITLE_1,
                TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);
        final Account account = accountDataMapper.transform(accountModel);

        assertEquals(TEST_ACCOUNT_ID_1, account.getId());
        assertEquals(TEST_ACCOUNT_TITLE_1, account.getName());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_1, account.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_1, account.getBalance(), 0.0);
    }

    @Test
    public void testShouldTransformModelListToDomainModelListCorrectly() throws Exception{
        final AccountModel accountModel1 = new AccountModel(TEST_ACCOUNT_ID_1, TEST_ACCOUNT_TITLE_1,
                TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);
        final AccountModel accountModel2 = new AccountModel(TEST_ACCOUNT_ID_2, TEST_ACCOUNT_TITLE_2,
                TEST_ACCOUNT_BALANCE_2, TEST_ACCOUNT_CURRENCY_ID_2);

        final List<AccountModel> accountModelList = new ArrayList<>();
        accountModelList.add(accountModel1);
        accountModelList.add(accountModel2);

        assertEquals(2 ,accountModelList.size());

        final List<Account> accountList = accountDataMapper.transform(accountModelList);

        assertEquals(2 ,accountList.size());

        final Account account1 = accountList.get(0);
        final Account account2 = accountList.get(1);

        assertEquals(TEST_ACCOUNT_ID_1, account1.getId());
        assertEquals(TEST_ACCOUNT_TITLE_1, account1.getName());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_1, account1.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_1, account1.getBalance(), 0.0);

        assertEquals(TEST_ACCOUNT_ID_2, account2.getId());
        assertEquals(TEST_ACCOUNT_TITLE_2, account2.getName());
        assertEquals((long) TEST_ACCOUNT_CURRENCY_ID_2, account2.getCurrency());
        assertEquals(TEST_ACCOUNT_BALANCE_2, account2.getBalance(), 0.0);
    }

}
