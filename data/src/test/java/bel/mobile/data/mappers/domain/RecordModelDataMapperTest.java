package bel.mobile.data.mappers.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.db.CategoryTable;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.model.Account;
import bel.mobile.data.model.Category;
import bel.mobile.data.model.Record;
import bel.mobile.domain.model.RecordModel;

import static org.junit.Assert.assertEquals;

public class RecordModelDataMapperTest {

    private RecordModelDataMapper recordModelDataMapper;

    @Before
    public void setUp() throws Exception {
        recordModelDataMapper = new RecordModelDataMapper();
    }

    @Test
    public void testTransformModelToDataModel() throws Exception {
        Record record = new Record(0, TransactionType.INCOME.INSTANCE, 15.4, "Description", 1, 1);
        Account account = new Account(10, "Debit", (double) 160, 1);
        Category category = new Category(5, "Food", TransactionType.EXPENSE.INSTANCE, CategoryTable.defaultCategoryId);

        record.account.setTarget(account);
        record.category.setTarget(category);

        RecordModel recordModel = recordModelDataMapper.transform(record);

        assertEquals(record.getType().type(), recordModel.getType());
        assertEquals(record.getAmount(), recordModel.getAmount(), 0.0);
        assertEquals(account.getId(), record.getAccountId());
        assertEquals(category.getId(), record.getCategoryId());
    }

    @Test
    public void testTransformModelListToDataModelList() throws Exception {
        Account account = new Account(10, "Debit", (double) 160, 1);
        Category category = new Category(5, "Food", TransactionType.EXPENSE.INSTANCE, CategoryTable.defaultCategoryId);

        Record record1 = new Record(3, TransactionType.EXPENSE.INSTANCE, 15.4, "Description");
        Record record2 = new Record(4, TransactionType.EXPENSE.INSTANCE, 16.7, "Description");
        Record record3 = new Record(5, TransactionType.EXPENSE.INSTANCE, 10, "Description");

        record1.account.setTarget(account);
        record1.category.setTarget(category);

        record2.account.setTarget(account);
        record2.category.setTarget(category);

        record3.account.setTarget(account);
        record3.category.setTarget(category);

        List<Record> recordList = new ArrayList<>();
        recordList.add(record1);
        recordList.add(record2);
        recordList.add(record3);

        List<RecordModel> recordModelList = recordModelDataMapper.transform(recordList);

        assertEquals(recordList.size(), recordModelList.size());

        assertEquals(recordList.get(0).getType().type(), recordModelList.get(0).getType());
        assertEquals(recordList.get(0).getAmount(), recordModelList.get(0).getAmount(), 0.0);
        assertEquals(recordList.get(0).getAccountId(), recordModelList.get(0).getAccountId());
        assertEquals(recordList.get(0).getCategoryId(), recordModelList.get(0).getCategoryId());

        assertEquals(recordList.get(1).getType().type(), recordModelList.get(1).getType());
        assertEquals(recordList.get(1).getAmount(), recordModelList.get(1).getAmount(), 0.0);
        assertEquals(recordList.get(1).getAccountId(), recordModelList.get(1).getAccountId());
        assertEquals(recordList.get(1).getCategoryId(), recordModelList.get(1).getCategoryId());

        assertEquals(recordList.get(2).getType().type(), recordModelList.get(2).getType());
        assertEquals(recordList.get(2).getAmount(), recordModelList.get(2).getAmount(), 0.0);
        assertEquals(recordList.get(2).getAccountId(), recordModelList.get(2).getAccountId());
        assertEquals(recordList.get(2).getCategoryId(), recordModelList.get(2).getCategoryId());
    }
}
