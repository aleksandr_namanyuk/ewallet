package bel.mobile.data.model;

import org.junit.Test;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.AccountTable;
import io.objectbox.Box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AccountTest extends AbstractObjectBoxTest {

    private Box<Account> accountBox;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        accountBox = getAccountBox();
        assertNotNull(accountBox);
    }

    @Test
    public void testShouldPutAndGetSuccessfully() {
        final Account account = new Account();
        account.setName("Debit");

        accountBox.put(account);

        final Account accountFromBox = accountBox.query().build().findFirst();
        assertNotNull(accountFromBox);
        assertEquals("Debit", accountFromBox.getName());
    }

    @Test
    public void testGetWithDefaultConstructorSuccessfully(){
        final Account account = new Account();
        final long id = accountBox.put(account);
        final Account accountFromBox = accountBox.query().build().findFirst();

        assertNotNull(accountFromBox);
        assertEquals(id, accountFromBox.getId());
        assertEquals("", accountFromBox.getName());
        assertEquals(AccountTable.defaultBalance, accountFromBox.getBalance(), 0.0);
        assertEquals((long) AccountTable.defaultCurrency, accountFromBox.getCurrency());
    }

    @Test
    public void testGetWithNullValuesInConstructorSuccessfully(){
        final Account account = new Account(0, "", 0.0, 1);
        long id = accountBox.put(account);
        final Account accountFromBox = accountBox.query().build().findFirst();

        assertNotNull(accountFromBox);
        assertEquals(id, accountFromBox.getId());
        assertEquals("", accountFromBox.getName());
        assertEquals(AccountTable.defaultBalance, accountFromBox.getBalance(), 0.0);
        assertEquals((long) AccountTable.defaultCurrency, accountFromBox.getCurrency());
    }

    @Test
    public void testShouldDeleteSuccessfully(){
        final Account account = new Account();
        final long id = accountBox.put(account);

        assertEquals(1, accountBox.count());
        accountBox.remove(id);
        assertEquals(0, accountBox.count());
    }

}
