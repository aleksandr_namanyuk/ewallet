package bel.mobile.data.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.customtypes.TransactionType;
import io.objectbox.Box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class CategoryTest extends AbstractObjectBoxTest {

    private Box<Category> categoryBox;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        categoryBox = getCategoryBox();
    }

    @Test
    public void testShouldSuccessfullyCheckRelations(){
        final Category category1 = new Category(0, "Food & Drinks");
        category1.setType(TransactionType.INCOME.INSTANCE);
        assertEquals(0, category1.subCategories.size());
        categoryBox.put(category1);

        final Category categoryFromBox = categoryBox.query().build().findFirst();

        assertNotNull(categoryFromBox);
        assertEquals("Food & Drinks", categoryFromBox.getTitle());
        assertEquals(TransactionType.INCOME.INSTANCE, categoryFromBox.getType());

        final Category category9 = new Category(0, "Groceries");
        categoryBox.attach(category9);
        category1.subCategories.add(category9);

        long categoryId = categoryBox.put(category1);
        Category categoryFromBox2 = categoryBox.get(categoryId);

        assertEquals(category1, category9.parent.getTarget());
        assertEquals(category1.getTitle(), category9.parent.getTarget().getTitle());

        assertEquals(1, categoryFromBox2.subCategories.size());
        assertEquals("Groceries", categoryFromBox2.subCategories.get(0).getTitle());
    }

    @Test
    public void testDefaultConstructor(){
        final Category category1 = new Category();
        categoryBox.put(category1);
        final Category categoryFromBox = categoryBox.query().build().findFirst();

        assertNotNull(categoryFromBox);
        assertEquals(1, categoryFromBox.getId());
        assertEquals("", categoryFromBox.getTitle());
        assertEquals(TransactionType.EXPENSE.INSTANCE, categoryFromBox.getType());

        assertNotNull(categoryFromBox.parent);
        assertNull(categoryFromBox.parent.getTarget());
        assertNotNull(categoryFromBox.subCategories);
        assertFalse(categoryFromBox.subCategories.isResolved());
    }

    @Test
    public void testRecordRelations() throws Exception {
        final Category category = new Category(0, "Food");

        final Record record1 = new Record(0, TransactionType.EXPENSE.INSTANCE, 173.8);
        final Record record2 = new Record(0, TransactionType.EXPENSE.INSTANCE, 36.2);
        final Record record3 = new Record(0, TransactionType.EXPENSE.INSTANCE, 17);
        final Record record4 = new Record(0, TransactionType.EXPENSE.INSTANCE, 188);
        final Record record5 = new Record(0, TransactionType.EXPENSE.INSTANCE, 198);
        final Record record6 = new Record(0, TransactionType.EXPENSE.INSTANCE, 189);
        final Record record7 = new Record(0, TransactionType.EXPENSE.INSTANCE, 130);
        final Record record8 = new Record(0, TransactionType.EXPENSE.INSTANCE, 18);
        final Record record9 = new Record(0, TransactionType.EXPENSE.INSTANCE, 88);
        final Record record10 = new Record(0, TransactionType.EXPENSE.INSTANCE, 89);
        final Record record11 = new Record(0, TransactionType.EXPENSE.INSTANCE, 29);
        final Record record12 = new Record(0, TransactionType.EXPENSE.INSTANCE, 63);
        final Record record13 = new Record(0, TransactionType.EXPENSE.INSTANCE, 99);
        final Record record14 = new Record(0, TransactionType.EXPENSE.INSTANCE, 77);
        final Record record15 = new Record(0, TransactionType.EXPENSE.INSTANCE, 21);
        final Record record16 = new Record(0, TransactionType.EXPENSE.INSTANCE, 3);
        final Record record17 = new Record(0, TransactionType.EXPENSE.INSTANCE, 42);
        final Record record18 = new Record(0, TransactionType.EXPENSE.INSTANCE, 1);
        final Record record19 = new Record(0, TransactionType.EXPENSE.INSTANCE, 425);
        final Record record20 = new Record(0, TransactionType.EXPENSE.INSTANCE, 123);

        assertNotNull(category.records);

        category.records.add(record1);
        category.records.add(record2);
        category.records.add(record3);
        category.records.add(record4);
        category.records.add(record5);
        category.records.add(record6);
        category.records.add(record7);
        category.records.add(record8);
        category.records.add(record9);
        category.records.add(record10);
        category.records.add(record11);
        category.records.add(record12);
        category.records.add(record13);
        category.records.add(record14);
        category.records.add(record15);
        category.records.add(record16);
        category.records.add(record17);
        category.records.add(record18);
        category.records.add(record19);
        category.records.add(record20);

        categoryBox.put(category);

        long start = System.currentTimeMillis();

        final Category foodCategory = categoryBox.query().equal(Category_.title, "Food").build().findFirst();

        assertNotNull(foodCategory);
        assertEquals(20, foodCategory.records.size());

        List<Record> expenseRecords = new ArrayList<>();
        for (Record record : foodCategory.records){
            if(record.getType() == TransactionType.EXPENSE.INSTANCE)
                expenseRecords.add(record);
        }

        long end = System.currentTimeMillis();
        System.out.println("DEBUG: Logic took " + (end - start) + " MilliSeconds");

        assertEquals(20, expenseRecords.size());
    }

    @Test
    public void testCategoryOrderByParent(){
        final Category category1 = new Category(0, "Food");
        final Category category2 = new Category(0, "Bar");
        final Category category3 = new Category(0, "Outside");

        final Category category4 = new Category(0, "Entertainment");
        final Category category5 = new Category(0, "Golf");
        final Category category6 = new Category(0, "Games");

        final Category category7 = new Category(0, "Car");

        category1.subCategories.add(category2);
        category1.subCategories.add(category3);

        category4.subCategories.add(category5);
        category4.subCategories.add(category6);

        categoryBox.put(category1);
        categoryBox.put(category4);
        categoryBox.put(category7);

        List<Category> categoryList = categoryBox.query().build().find();

        assertEquals(7, categoryList.size());

        List<Category> categoryListWithoutParents = categoryBox.query().equal(Category_.parentId, 0).build().find();
        assertEquals(3, categoryListWithoutParents.size());

        Category categoryFood = categoryBox.query().equal(Category_.title, "Food").build().findUnique();
        assertNotNull(categoryFood);
        List<Category> categoryListWithParent1 = categoryBox.query().equal(Category_.parentId, categoryFood.getId()).build().find();
        assertEquals(2, categoryListWithParent1.size());

    }

}
