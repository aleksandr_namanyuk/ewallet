package bel.mobile.data.model;

import org.junit.Test;

import java.util.List;

import bel.mobile.data.AbstractObjectBoxTest;
import bel.mobile.data.db.customtypes.TransactionType;
import io.objectbox.Box;

import static bel.mobile.data.DataTestData.TEST_ACCOUNT_TITLE_2;
import static bel.mobile.data.DataTestData.TEST_CATEGORY_TITLE_1;
import static bel.mobile.data.DataTestData.TEST_NEW_RECORD;
import static bel.mobile.data.DataTestData.TEST_RECORD_AMOUNT_1;
import static bel.mobile.data.DataTestData.TEST_RECORD_TYPE_1;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class RecordTest extends AbstractObjectBoxTest {

    private Box<Record> recordBox;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        recordBox = getRecordBox();
    }

    @Test
    public void testDefaultConstructorValues() throws Exception{
        final Record record = new Record();
        long id = recordBox.put(record);

        final Record recordFromBox = recordBox.get(id);

        assertEquals(TransactionType.EXPENSE.INSTANCE, recordFromBox.getType());
        assertEquals(0.0, recordFromBox.getAmount(), 0.0);
        assertEquals("", recordFromBox.getDescription());
    }

    @Test
    public void testConstructorWithAmountOnly() throws Exception{
        final Record record = new Record(0, TransactionType.INCOME.INSTANCE,15.5);
        long id = recordBox.put(record);

        final Record recordFromBox = recordBox.get(id);

        assertEquals(TransactionType.INCOME.INSTANCE, recordFromBox.getType());
        assertEquals(15.5, recordFromBox.getAmount(), 0.0);
        assertEquals("", recordFromBox.getDescription());
    }

    @Test
    public void testRelationsWithAccount() throws Exception{
        final Record record = TEST_NEW_RECORD;
        assertNotNull(record.account);

        final Account account = new Account();
        account.setName(TEST_ACCOUNT_TITLE_2);

        record.account.setTarget(account);
        final long id = recordBox.put(record);

        final Record recordFromBox = recordBox.get(id);

        assertEquals(TEST_RECORD_AMOUNT_1, recordFromBox.getAmount(), 0.0);

        assertNotNull(recordFromBox.account);
        assertTrue(recordFromBox.account.getTargetId() > 0);

        assertNotNull(recordFromBox.account.getTarget());
        final Account accountFromBox = recordFromBox.account.getTarget();
        assertNotNull(accountFromBox);
        assertEquals(TEST_ACCOUNT_TITLE_2, accountFromBox.getName());
    }

    @Test
    public void testRelationsWithCategory() throws Exception{
        final Record record = TEST_NEW_RECORD;
        record.setId(0L);

        assertNotNull(record.category);

        final Category category = new Category();
        category.setTitle(TEST_CATEGORY_TITLE_1);
        category.setType(TransactionType.INCOME.INSTANCE);
        record.category.setTarget(category);
        final long id = recordBox.put(record);
        final Record recordFromBox = recordBox.get(id);

        assertEquals(TEST_RECORD_AMOUNT_1, recordFromBox.getAmount(), 0.0);
        assertNotNull(recordFromBox.category);
        assertTrue(recordFromBox.category.getTargetId() > 0);
        assertNotNull(recordFromBox.category.getTarget());

        final Category categoryFromBox = recordFromBox.category.getTarget();

        assertNotNull(categoryFromBox);
        assertEquals(TEST_CATEGORY_TITLE_1, categoryFromBox.getTitle());
        assertEquals(TransactionType.INCOME.INSTANCE, categoryFromBox.getType());

        recordBox.removeAll();
    }

    @Test
    public void testTransactionType() throws Exception{
        TEST_NEW_RECORD.setId(0L);
        assertEquals(0L, TEST_NEW_RECORD.getId());

        long id = recordBox.put(TEST_NEW_RECORD);

        final Record recordFromBox = recordBox.get(id);
        assertEquals(recordFromBox.getType(), TEST_RECORD_TYPE_1);

        TransactionType updatedTransactionType = TransactionType.INCOME.INSTANCE;
        recordFromBox.setType(updatedTransactionType);

        long idAfterUpdate = recordBox.put(recordFromBox);
        final Record recordFromBoxAfterUpdate = recordBox.get(idAfterUpdate);

        assertEquals(id, idAfterUpdate);
        assertEquals(updatedTransactionType, recordFromBoxAfterUpdate.getType());
    }

    @Test
    public void testCategoryRelations() throws Exception {
        final Box<Category> categoryBox = getCategoryBox();
        final Category category = new Category(0, "Food");

        final Record record1 = new Record(0, TransactionType.EXPENSE.INSTANCE, 173.8);
        record1.category.setTarget(category);
        recordBox.put(record1);

        final Record record2 = new Record(0, TransactionType.EXPENSE.INSTANCE, 36.2);
        record2.category.setTarget(category);
        recordBox.put(record2);

        final Record record3 = new Record(0, TransactionType.EXPENSE.INSTANCE, 17);
        record3.category.setTarget(category);
        recordBox.put(record3);

        final Record record4 = new Record(0, TransactionType.EXPENSE.INSTANCE, 188);
        record4.category.setTarget(category);
        recordBox.put(record4);

        final Record record5 = new Record(0, TransactionType.EXPENSE.INSTANCE, 198);
        record5.category.setTarget(category);
        recordBox.put(record5);

        final Record record6 = new Record(0, TransactionType.EXPENSE.INSTANCE, 189);
        record6.category.setTarget(category);
        recordBox.put(record6);

        final Record record7 = new Record(0, TransactionType.EXPENSE.INSTANCE, 130);
        record7.category.setTarget(category);
        recordBox.put(record7);

        final Record record8 = new Record(0, TransactionType.EXPENSE.INSTANCE, 18);
        record8.category.setTarget(category);
        recordBox.put(record8);

        final Record record9 = new Record(0, TransactionType.EXPENSE.INSTANCE, 88);
        record9.category.setTarget(category);
        recordBox.put(record9);

        final Record record10 = new Record(0, TransactionType.EXPENSE.INSTANCE, 89);
        record10.category.setTarget(category);
        recordBox.put(record10);

        final Record record11 = new Record(0, TransactionType.EXPENSE.INSTANCE, 29);
        record11.category.setTarget(category);
        recordBox.put(record11);

        final Record record12 = new Record(0, TransactionType.EXPENSE.INSTANCE, 63);
        record12.category.setTarget(category);
        recordBox.put(record12);

        final Record record13 = new Record(0, TransactionType.EXPENSE.INSTANCE, 99);
        record13.category.setTarget(category);
        recordBox.put(record13);

        final Record record14 = new Record(0, TransactionType.EXPENSE.INSTANCE, 77);
        record14.category.setTarget(category);
        recordBox.put(record14);

        final Record record15 = new Record(0, TransactionType.EXPENSE.INSTANCE, 21);
        record15.category.setTarget(category);
        recordBox.put(record15);

        final Record record16 = new Record(0, TransactionType.EXPENSE.INSTANCE, 3);
        record16.category.setTarget(category);
        recordBox.put(record16);

        final Record record17 = new Record(0, TransactionType.EXPENSE.INSTANCE, 42);
        record17.category.setTarget(category);
        recordBox.put(record17);

        final Record record18 = new Record(0, TransactionType.EXPENSE.INSTANCE, 1);
        record18.category.setTarget(category);
        recordBox.put(record18);

        final Record record19 = new Record(0, TransactionType.EXPENSE.INSTANCE, 425);
        record19.category.setTarget(category);
        recordBox.put(record19);

        final Record record20 = new Record(0, TransactionType.EXPENSE.INSTANCE, 123);
        record20.category.setTarget(category);
        recordBox.put(record20);

        assertNotNull(category.records);

        categoryBox.put(category);

        long start = System.currentTimeMillis();

        final Category foodCategory = categoryBox.query().equal(Category_.title, "Food").build().findFirst();

        assertNotNull(foodCategory);
        assertEquals(20, foodCategory.records.size());

        final List<Record> recordList = recordBox.query()
                .equal(Record_.categoryId, foodCategory.getId())
                .equal(Record_.type, TransactionType.EXPENSE.INSTANCE.type())
                .build().find();

        long end = System.currentTimeMillis();
        System.out.println("DEBUG: Logic took " + (end - start) + " MilliSeconds");

        assertEquals(20, recordList.size());
    }

    @Test
    public void testRecordNewEntityCreation() throws Exception {
        final Box<Account> accountBox = getAccountBox();
        final Box<Category> categoryBox = getCategoryBox();

        final long accountId = accountBox.put(new Account(0, "Debit", 156, 1));
        final long categoryId = categoryBox.put(new Category(0, "Food", TransactionType.EXPENSE.INSTANCE, 0));

        final Record record = new Record(0, TransactionType.EXPENSE.INSTANCE, 15.5);
        long recordId = recordBox.put(record);

        final Record recordFromBox = recordBox.get(recordId);

        assertEquals(record.getAmount(), recordFromBox.getAmount(), 0.0);

        assertNull(record.account.getTarget());
        assertNull(record.category.getTarget());

        recordFromBox.setCategoryId(categoryId);
        long recordWithCategory = recordBox.put(recordFromBox);

        final Record recordFromBoxWithCategory = recordBox.get(recordWithCategory);
        assertNotNull(recordFromBoxWithCategory.category.getTarget());
        assertEquals(categoryId, recordFromBoxWithCategory.category.getTarget().getId());
        assertEquals("Food", recordFromBoxWithCategory.category.getTarget().getTitle());
        assertEquals(TransactionType.EXPENSE.INSTANCE, recordFromBoxWithCategory.category.getTarget().getType());
    }


}
