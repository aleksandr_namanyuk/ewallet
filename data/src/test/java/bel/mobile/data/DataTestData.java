package bel.mobile.data;

import bel.mobile.data.db.CategoryTable;
import bel.mobile.data.db.customtypes.TransactionType;
import bel.mobile.data.model.Account;
import bel.mobile.data.model.Category;
import bel.mobile.data.model.Record;
import bel.mobile.domain.model.AccountModel;
import bel.mobile.domain.model.CategoryModel;

public class DataTestData {

    public static final long TEST_ACCOUNT_ID_1 = 15L;
    public static final String TEST_ACCOUNT_TITLE_1 = "Debit";
    public static final Double TEST_ACCOUNT_BALANCE_1 = 150.4;
    public static final Integer TEST_ACCOUNT_CURRENCY_ID_1 = 1;

    public static final long TEST_ACCOUNT_ID_2 = 18L;
    public static final String TEST_ACCOUNT_TITLE_2 = "Credit";
    public static final Double TEST_ACCOUNT_BALANCE_2 = 10.6;
    public static final Integer TEST_ACCOUNT_CURRENCY_ID_2 = 2;

    public static final long TEST_CATEGORY_ID_1 = 10L;
    public static final String TEST_CATEGORY_TITLE_1 = "Bar";
    public static final TransactionType TEST_CATEGORY_TYPE_1 = TransactionType.INCOME.INSTANCE;

    public static final CategoryModel TEST_CATEGORY_MODEL = new CategoryModel(1L, "Food", 0);
    public static final Category TEST_CATEGORY = new Category(1L, "Food",
            TransactionType.INCOME.INSTANCE, CategoryTable.defaultCategoryId);

    public static final Account TEST_ACCOUNT = new Account(TEST_ACCOUNT_ID_1, TEST_ACCOUNT_TITLE_1,
            TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);

    public static final AccountModel TEST_ACCOUNT_MODEL = new AccountModel(TEST_ACCOUNT_ID_1, TEST_ACCOUNT_TITLE_1,
            TEST_ACCOUNT_BALANCE_1, TEST_ACCOUNT_CURRENCY_ID_1);

    public static final long TEST_NEW_RECORD_ID = 0L;
    public static final TransactionType TEST_RECORD_TYPE_1 = TransactionType.EXPENSE.INSTANCE;
    public static final String TEST_RECORD_NAME = "Food";
    public static final Double TEST_RECORD_AMOUNT_1 = 150.5;
    public static final String TEST_RECORD_DESCRIPTION = "Food for one week for 5 people";

    public static final Record TEST_NEW_RECORD = new Record(TEST_NEW_RECORD_ID, TEST_RECORD_TYPE_1,
            TEST_RECORD_AMOUNT_1, TEST_RECORD_DESCRIPTION, 1, 1);


}
