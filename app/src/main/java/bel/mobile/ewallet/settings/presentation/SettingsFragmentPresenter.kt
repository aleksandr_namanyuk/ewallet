package bel.mobile.ewallet.settings.presentation

import bel.mobile.ewallet.base.BasePresenter

interface SettingsFragmentPresenter: BasePresenter {

    fun createCurrencyListPreference()
}