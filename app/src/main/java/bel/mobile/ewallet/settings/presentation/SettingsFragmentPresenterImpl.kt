package bel.mobile.ewallet.settings.presentation

import bel.mobile.ewallet.settings.view.SettingsView
import java.util.*
import javax.inject.Inject

class SettingsFragmentPresenterImpl @Inject constructor(val view: SettingsView): SettingsFragmentPresenter {

    override fun bind() {

    }

    override fun unBind() {

    }

    override fun createCurrencyListPreference() {
        val currencies = Currency.getAvailableCurrencies()
        val entries = ArrayList<CharSequence>(currencies.size)
        val values = ArrayList<CharSequence>(currencies.size)

        for(currency in currencies){
            entries.add("${currency.displayName} - ${currency.symbol}")
            values.add(currency.currencyCode)
        }

        view.initCurrencyList(entries, values)
    }
}