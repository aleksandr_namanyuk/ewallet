package bel.mobile.ewallet.settings.view

import android.content.Context
import android.os.Bundle
import android.support.v7.preference.ListPreference
import bel.mobile.ewallet.R
import bel.mobile.ewallet.settings.presentation.SettingsFragmentPresenter
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import kotlin.collections.ArrayList

class SettingsFragment: PreferenceFragmentCompat(), SettingsView {

    override fun onCreatePreferencesFix(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        presenter.createCurrencyListPreference()
    }

    @Inject
    lateinit var presenter: SettingsFragmentPresenter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        showTitle()
        super.onAttach(context)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences)
        presenter.createCurrencyListPreference()
    }

    override fun initCurrencyList(entries: ArrayList<CharSequence>, values: ArrayList<CharSequence>){
        val currencyList = findPreference("keyCurrency") as ListPreference?
        currencyList?.entries = entries.toTypedArray()
        currencyList?.entryValues = values.toTypedArray()
    }

    override fun showTitle() {
        activity?.title = getString(R.string.title_settings)
    }
}