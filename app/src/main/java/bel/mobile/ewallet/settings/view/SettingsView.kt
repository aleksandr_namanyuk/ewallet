package bel.mobile.ewallet.settings.view

import bel.mobile.ewallet.base.HasTitle

interface SettingsView: HasTitle {

    fun initCurrencyList(entries: ArrayList<CharSequence>, values: ArrayList<CharSequence>)
}