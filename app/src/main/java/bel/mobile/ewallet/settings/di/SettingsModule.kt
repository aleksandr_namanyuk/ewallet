package bel.mobile.ewallet.settings.di

import bel.mobile.ewallet.settings.presentation.SettingsFragmentPresenter
import bel.mobile.ewallet.settings.view.SettingsFragment
import bel.mobile.ewallet.settings.presentation.SettingsFragmentPresenterImpl
import bel.mobile.ewallet.settings.view.SettingsView
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class SettingsModule {

    @Binds
    abstract fun provideSettingsView(fragment: SettingsFragment): SettingsView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideSettingsActivityPresenter(view: SettingsView): SettingsFragmentPresenter =
                SettingsFragmentPresenterImpl(view)
    }
}