package bel.mobile.ewallet.main.di

import bel.mobile.ewallet.main.presentation.MainActivityPresenter
import bel.mobile.ewallet.main.presentation.MainActivityPresenterImpl
import bel.mobile.ewallet.main.view.MainActivity
import bel.mobile.ewallet.main.view.MainActivityView
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class MainActivityModule{

    @Binds
    @MainActivityScope
    abstract fun provideMainActivityView(mainActivity: MainActivity): MainActivityView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideMainActivityPresenter(view: MainActivityView): MainActivityPresenter =
                MainActivityPresenterImpl(view)
    }
}