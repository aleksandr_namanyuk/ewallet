package bel.mobile.ewallet.main.di

import bel.mobile.ewallet.accounts.view.AccountListFragment
import bel.mobile.ewallet.accounts.di.AccountListModule
import bel.mobile.ewallet.categories.view.CategoryListFragment
import bel.mobile.ewallet.categories.di.CategoryListModule
import bel.mobile.ewallet.record.di.RecordFragmentModel
import bel.mobile.ewallet.record.view.RecordFragment
import bel.mobile.ewallet.settings.view.SettingsFragment
import bel.mobile.ewallet.settings.di.SettingsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Main Activity Module with injected dependencies.
 */
@Module
abstract class MenuModule {

    @ContributesAndroidInjector(modules = [AccountListModule::class])
    abstract fun provideAccountListFragment(): AccountListFragment

    @ContributesAndroidInjector(modules = [CategoryListModule::class])
    abstract fun provideCategoryListFragment(): CategoryListFragment

    @ContributesAndroidInjector(modules = [RecordFragmentModel::class])
    abstract fun provideRecordFragment(): RecordFragment

    @ContributesAndroidInjector(modules = [SettingsModule::class])
    abstract fun provideSettingsFragment(): SettingsFragment
}