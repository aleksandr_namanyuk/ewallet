package bel.mobile.ewallet.main.presentation

import bel.mobile.ewallet.main.view.MainActivityView
import javax.inject.Inject

class MainActivityPresenterImpl @Inject constructor(private val view: MainActivityView): MainActivityPresenter {

    override fun onRecordButtonClick() {
        view.loadRecordFragment()
    }

    override fun onCategoryButtonClick() {
        view.loadCategoriesFragment()
    }

    override fun onHomeClick() {
        view.loadHomeFragment()
    }

    override fun onSettingsClick() {
        view.loadSettingsFragment()
    }
}