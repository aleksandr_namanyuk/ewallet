package bel.mobile.ewallet.main.view

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import bel.mobile.ewallet.R
import bel.mobile.ewallet.accounts.view.AccountListFragment
import bel.mobile.ewallet.categories.view.CategoryListFragment
import bel.mobile.ewallet.main.presentation.MainActivityPresenter
import bel.mobile.ewallet.record.view.RecordFragment
import bel.mobile.ewallet.settings.view.SettingsFragment
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainActivityView {

    @Inject lateinit var presenter: MainActivityPresenter

//    TODO: to Navigation class
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_record -> {
                presenter.onRecordButtonClick()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_categories -> {
                presenter.onCategoryButtonClick()
//                message.setText(R.string.title_home)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_accounts -> {
                presenter.onHomeClick()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                presenter.onSettingsClick()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    override fun loadRecordFragment(){
        loadFragment(RecordFragment().newInstance())
    }

    override fun loadCategoriesFragment() {
        loadFragment(CategoryListFragment())
    }

    override fun loadHomeFragment() {
        loadFragment(AccountListFragment())
    }

    override fun loadSettingsFragment() {
        loadFragment(SettingsFragment())
    }

//    TODO : create an external function for Navigation
    private fun loadFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainFrame, fragment)
                .addToBackStack(null)
                .commit()
    }


}
