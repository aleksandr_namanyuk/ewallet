package bel.mobile.ewallet.main.di

import javax.inject.Scope

/**
 * Scope for the MainActivity which is Menu activity.
 * Should be dependent on component in User Scope and
 * should have sub components responsible for Fragments.
 *
 * Created by bel on 10/02/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainActivityScope