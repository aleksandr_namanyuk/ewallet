package bel.mobile.ewallet.main.view

import bel.mobile.ewallet.base.BaseView

interface MainActivityView: BaseView{

    fun loadRecordFragment()
    fun loadCategoriesFragment()
    fun loadHomeFragment()
    fun loadSettingsFragment()
}