package bel.mobile.ewallet.main.presentation

/**
 * Presenter interface for the Main menu activity.
 */
interface MainActivityPresenter{

    fun onRecordButtonClick()
    fun onCategoryButtonClick()
    fun onHomeClick()
    fun onSettingsClick()
}