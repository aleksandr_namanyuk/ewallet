package bel.mobile.ewallet

import bel.mobile.data.db.interactor.categories.PrepopulateCategories
import bel.mobile.data.preferences.AppPreferences
import bel.mobile.ewallet.di.AppComponent
import bel.mobile.ewallet.di.DaggerAppComponent
import com.squareup.leakcanary.LeakCanary
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

/**
 * Main application class.
 * Dagger2 injection.
 */
class WalletApplication: DaggerApplication() {

    @Inject lateinit var appPreferences: AppPreferences
    @Inject lateinit var prepopulateCategories: PrepopulateCategories

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val applicationComponent: AppComponent = DaggerAppComponent.builder()
                .application(this)
                .build()
        applicationComponent.inject(this)
        return applicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        installLeakCanary()
        populateDatabaseWithDefaultData()
    }

    private fun installLeakCanary(){
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        LeakCanary.install(this)
    }

    private fun populateDatabaseWithDefaultData(){
        if(appPreferences.isFirstAppLaunch()){
            prepopulateCategories.createCompletable()
            appPreferences.appFirstLaunchDone()
        }
    }
}