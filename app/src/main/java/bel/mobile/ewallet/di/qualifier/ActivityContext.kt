package bel.mobile.ewallet.di.qualifier

import javax.inject.Qualifier

/**
 * Context of the activity. SHould be used for internal scopes.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityContext