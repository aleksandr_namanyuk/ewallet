package bel.mobile.ewallet.di

import bel.mobile.ewallet.WalletApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Main Application Component of Dagger2 for the whole Application level.
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    DataModule::class,
    DatabaseModule::class,
    ActivityBindingModule::class])
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        /**
         * Allows the instance of Application to be injected in the AppComponent component
         * and be used in AppModule without injecting it to the constructor.
         */
        @BindsInstance
        fun application(application: WalletApplication): Builder

        fun build(): AppComponent
    }

    fun inject(application: WalletApplication)

    override fun inject(instance: DaggerApplication)
}