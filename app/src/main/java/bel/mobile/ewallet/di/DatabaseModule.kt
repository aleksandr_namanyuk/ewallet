package bel.mobile.ewallet.di

import android.content.Context
import bel.mobile.data.db.interactor.categories.PrepopulateCategories
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.source.local.AccountDao
import bel.mobile.data.source.local.AccountDaoImpl
import bel.mobile.data.source.local.CategoryDao
import bel.mobile.data.source.local.CategoryLocalDataSource
import bel.mobile.data.model.MyObjectBox
import bel.mobile.data.source.repository.CategoryRepository
import dagger.Module
import dagger.Provides
import io.objectbox.BoxStore
import javax.inject.Singleton

/**
 * Module for database tables.
 */
@Module
class DatabaseModule{

    @Provides
    @Singleton
    fun provideObjectBox(context: Context): BoxStore =
            MyObjectBox.builder()
                    .androidContext(context)
                    .build()

    @Provides
    @Singleton
    fun provideAccountDao(boxStore: BoxStore): AccountDao =
            AccountDaoImpl(boxStore)

    @Provides
    @Singleton
    fun provideCategoryDao(boxStore: BoxStore): CategoryDao =
            CategoryLocalDataSource(boxStore)

    @Provides
    @Singleton
    fun providePrepopulateCategories(categoryRepository: CategoryRepository,
                                     threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread): PrepopulateCategories =
            PrepopulateCategories(categoryRepository, threadExecutor, postExecutionThread)

}