package bel.mobile.ewallet.di

import android.content.Context
import android.content.SharedPreferences
import bel.mobile.data.preferences.AppPreferences
import bel.mobile.data.source.local.AccountDao
import bel.mobile.data.source.local.CategoryDao
import bel.mobile.data.source.repository.AccountRepository
import bel.mobile.data.source.repository.CategoryRepository
import bel.mobile.data.source.repository.base.AccountDataSource
import bel.mobile.data.source.repository.base.CategoryDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Module for the Data module
 */
@Module
class DataModule {

    companion object {
        const val APP_PREFS = "app_prefs"
    }

    @Provides
    @Singleton
    fun provideAccountRepository(accountDao: AccountDao): AccountDataSource =
            AccountRepository(accountDao)

    @Provides
    @Singleton
    fun provideCategoryRepository(dao: CategoryDao): CategoryDataSource =
            CategoryRepository(dao)

    @Provides
    @Singleton
    fun provideAppSharedPreferences(context: Context): SharedPreferences =
            context.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideAppPreferenceManager(preferences: SharedPreferences) =
            AppPreferences(preferences)

}