package bel.mobile.ewallet.di

import bel.mobile.ewallet.account.view.AccountActivity
import bel.mobile.ewallet.account.di.AccountActivityScope
import bel.mobile.ewallet.account.di.AccountModule
import bel.mobile.ewallet.main.view.MainActivity
import bel.mobile.ewallet.main.di.MainActivityModule
import bel.mobile.ewallet.main.di.MainActivityScope
import bel.mobile.ewallet.main.di.MenuModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * All activities mapped here.
 */
@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = [MenuModule::class, MainActivityModule::class])
    @MainActivityScope
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [AccountModule::class])
    @AccountActivityScope
    abstract fun provideAccountActivity(): AccountActivity
}