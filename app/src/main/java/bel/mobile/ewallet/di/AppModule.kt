package bel.mobile.ewallet.di

import android.content.Context
import bel.mobile.data.executor.JobExecutor
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.ewallet.UIThread
import bel.mobile.ewallet.WalletApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Dependency Injection Dagger2 Application Module.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: WalletApplication): Context =
            application.applicationContext

    @Provides
    @Singleton
    fun provideJobExecutor() = JobExecutor()

    @Provides
    @Singleton
    fun provideUIThread() = UIThread()

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor =
            jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread =
            uiThread


}
