package bel.mobile.ewallet.accounts.di

import bel.mobile.data.source.repository.base.AccountDataSource
import bel.mobile.data.db.interactor.accounts.GetAccountsList
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.ewallet.accounts.presentation.AccountListPresenter
import bel.mobile.ewallet.accounts.presentation.AccountListPresenterImpl
import bel.mobile.ewallet.accounts.presentation.AccountsAdapterPresenter
import bel.mobile.ewallet.accounts.presentation.AccountsAdapterPresenterImpl
import bel.mobile.ewallet.accounts.view.AccountListFragment
import bel.mobile.ewallet.accounts.view.AccountListView
import bel.mobile.ewallet.accounts.view.AccountsAdapter
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Account List module.
 */
@Module
abstract class AccountListModule{

    @Binds
    abstract fun provideAccountListView(accountListFragment: AccountListFragment): AccountListView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideGetAllAccountsUseCase(accountRepository: AccountDataSource,
                                         threadExecutor: ThreadExecutor,
                                         postExecutionThread: PostExecutionThread) =
                GetAccountsList(accountRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun provideAccountListPresenter(view: AccountListView,
                                        getAccounts: GetAccountsList): AccountListPresenter =
                AccountListPresenterImpl(view, getAccounts)

        @JvmStatic
        @Provides
        fun provideAccountListAdapter(presenter: AccountsAdapterPresenter): AccountsAdapter =
                AccountsAdapter(presenter)

        @JvmStatic
        @Provides
        fun provideAccountAdapterPresenter(view: AccountListView): AccountsAdapterPresenter =
                AccountsAdapterPresenterImpl(view)

    }

}