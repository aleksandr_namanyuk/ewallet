package bel.mobile.ewallet.accounts.view

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import bel.mobile.data.model.Account
import bel.mobile.ewallet.R
import bel.mobile.ewallet.accounts.presentation.AccountListPresenter
import bel.mobile.ewallet.navigation.navigateToCreateAccountScreen
import bel.mobile.ewallet.navigation.navigateToEditAccountScreen
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_accounts.*
import javax.inject.Inject

class AccountListFragment: DaggerFragment(), AccountListView{

    @Inject lateinit var accountsAdapter: AccountsAdapter
    @Inject lateinit var presenter: AccountListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_accounts, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind()
        presenter.loadAccounts()
    }

    override fun onResume() {
        super.onResume()
        presenter.loadAccounts()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.account, menu)
        menu.setGroupVisible(R.id.gAccountList, true)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.add) {
            presenter.onCreateAccountClicked()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initAdapter() {
        accounts.adapter = accountsAdapter
        accounts.layoutManager = LinearLayoutManager(activity)
        accounts.setHasFixedSize(true)
        accounts.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
    }

    override fun showTitle() {
        activity?.title = getString(R.string.accountsTitle)
    }

//    TODO: use DiffUtil
    override fun showAccounts(accounts: MutableList<Account>) {
        accountsAdapter.entities = accounts
        accountsAdapter.notifyDataSetChanged()
    }

    override fun openCreateAccountScreen() {
        this.navigateToCreateAccountScreen()
    }

    override fun openEditAccountScreen(accountId: Long) {
        this.navigateToEditAccountScreen(accountId)
    }

}