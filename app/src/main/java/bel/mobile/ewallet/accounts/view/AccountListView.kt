package bel.mobile.ewallet.accounts.view

import bel.mobile.data.model.Account
import bel.mobile.ewallet.base.BaseView

/**
 * View for fragment with accounts list.
 * Should include functions to manage UI for the list of accounts.
 */
interface AccountListView: BaseView {

    fun initAdapter()

    fun showTitle()
    fun showAccounts(accounts: MutableList<Account>)

    fun openCreateAccountScreen()
    fun openEditAccountScreen(accountId: Long)
}