//package bel.mobile.ewallet.accounts.view;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.DividerItemDecoration;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
//import android.view.View;
//import android.view.ViewGroup;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import bel.mobile.data.model.Account;
//import bel.mobile.ewallet.R;
//import bel.mobile.ewallet.account.view.AccountActivity;
//import bel.mobile.ewallet.accounts.presentation.AccountListPresenter;
//import dagger.android.support.DaggerFragment;
//
//import static bel.mobile.ewallet.account.view.AccountActivity.EXTRA_ACCOUNT_ID;
//
///**
// * Fragment to show list of accounts.
// */
//public class AccountListFragment extends DaggerFragment implements AccountListView {
//
//    @Inject AccountsAdapter accountsAdapter;
//    @Inject AccountListPresenter presenter;
////    @Inject Navigator navigator;
//    private RecyclerView accounts;
//
//    public static AccountListFragment newInstance() {
//        return new AccountListFragment();
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        final View view = inflater.inflate(R.layout.fragment_accounts, container, false);
//        setHasOptionsMenu(true);
//        accounts = view.findViewById(R.id.accounts);
//        return view;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        presenter.bind();
//        presenter.loadAccounts();
//    }
//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.account, menu);
//        menu.setGroupVisible(R.id.gAccountList, true);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId() == R.id.add){
//            presenter.onCreateAccountClicked();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override
//    public void initAdapter() {
//        accounts.setAdapter(accountsAdapter);
//        accounts.setLayoutManager(new LinearLayoutManager(getContext()));
//        accounts.setHasFixedSize(true);
//        accounts.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        presenter.loadAccounts();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        presenter.unBind();
//    }
//
//    @Override
//    public void showTitle() {
//        getActivity().setTitle(R.string.accountsTitle);
//    }
//
//    @Override
//    public void showAccounts(@NotNull List<Account> accounts) {
//        accountsAdapter.setEntities(accounts);
//        accountsAdapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void openCreateAccountScreen() {
//        startActivity(new Intent(getContext(), AccountActivity.class));
//    }
//
//    @Override
//    public void openEditAccountScreen(final long accountId) {
//        final Intent intent = new Intent(getContext(), AccountActivity.class);
//        intent.putExtra(EXTRA_ACCOUNT_ID, accountId);
//        startActivity(intent);
//    }
//
//}
