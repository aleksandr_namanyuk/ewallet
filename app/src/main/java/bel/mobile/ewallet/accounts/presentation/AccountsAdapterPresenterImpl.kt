package bel.mobile.ewallet.accounts.presentation

import bel.mobile.ewallet.accounts.view.AccountListView
import javax.inject.Inject

/**
 * Presenter implementation for the Adapter responsible for list of accounts.
 */
class AccountsAdapterPresenterImpl @Inject constructor(private val view: AccountListView): AccountsAdapterPresenter {

    override fun onAccountClicked(accountId: Long) {
        view.openEditAccountScreen(accountId)
    }

    override fun onAccountLongClick() {
    }

}