package bel.mobile.ewallet.accounts.presentation

import bel.mobile.ewallet.base.BasePresenter

/**
 * Presenter for account list fragment. Contains list of functionality in the fragment,
 * that should be handled before/after actions in the UI.
 */
interface AccountListPresenter: BasePresenter {

    fun loadAccounts()
    fun onCreateAccountClicked()
}