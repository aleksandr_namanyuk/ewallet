package bel.mobile.ewallet.accounts.presentation

import bel.mobile.data.db.interactor.accounts.GetAccountsList
import bel.mobile.data.model.Account
import bel.mobile.ewallet.accounts.view.AccountListView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject


/**
 * Account list presenter implementation.
 */
class AccountListPresenterImpl @Inject constructor(private val view: AccountListView,
                                                   private val getAccounts: GetAccountsList): AccountListPresenter {

    override fun bind() {
        view.showTitle()
        view.initAdapter()
    }

    override fun loadAccounts(){
        getAccounts.execute(GetAllAccountsObserver())
    }

    override fun unBind(){
        getAccounts.dispose()
    }

    override fun onCreateAccountClicked() {
        view.openCreateAccountScreen()
    }

    inner class GetAllAccountsObserver: DisposableSingleObserver<MutableList<Account>>(){

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

        override fun onSuccess(t: MutableList<Account>) {
            view.showAccounts(t)
        }
    }

}