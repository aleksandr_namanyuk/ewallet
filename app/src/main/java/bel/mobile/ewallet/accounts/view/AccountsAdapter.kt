package bel.mobile.ewallet.accounts.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bel.mobile.data.model.Account
import bel.mobile.ewallet.R
import bel.mobile.ewallet.accounts.presentation.AccountsAdapterPresenter
import bel.mobile.ewallet.base.BaseAdapter
import kotlinx.android.synthetic.main.item_account.view.*
import javax.inject.Inject

class AccountsAdapter @Inject constructor(private val presenter: AccountsAdapterPresenter):
        BaseAdapter<Account>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_account, parent, false)
        return ViewHolder(view)
    }

    inner class ViewHolder constructor(itemView: View): BaseViewHolder(itemView){

        override fun bind(item : Account){
            with(itemView){

                accountName.text = item.name
                accountBalance.text = item.balance.toString()

                setOnClickListener{
                    presenter.onAccountClicked(item.id)
                }

//                setOnLongClickListener {
//                    presenter.onAccountLongClick()
//                    return@setOnLongClickListener true
//                }

            }
        }

    }

}