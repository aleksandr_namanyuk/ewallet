package bel.mobile.ewallet.accounts.presentation

/**
 * Presenter listener for adapter to handle list of accounts.
 */
interface AccountsAdapterPresenter {

    fun onAccountClicked(accountId: Long)
    fun onAccountLongClick()
}