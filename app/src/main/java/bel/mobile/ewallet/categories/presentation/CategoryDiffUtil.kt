package bel.mobile.ewallet.categories.presentation

import android.support.v7.util.DiffUtil
import bel.mobile.data.model.Category

class CategoryDiffUtil(private val oldList: MutableList<Category>,
                       private val newList: MutableList<Category>): DiffUtil.Callback(){

    override fun getOldListSize(): Int =
            oldList.size

    override fun getNewListSize(): Int =
            newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCategory = oldList[oldItemPosition]
        val newCategory = newList[newItemPosition]
        return oldCategory.id == newCategory.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldCategory = oldList[oldItemPosition]
        val newCategory = newList[newItemPosition]
        return oldCategory == newCategory
    }

}