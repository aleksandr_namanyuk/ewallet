package bel.mobile.ewallet.categories.presentation

import bel.mobile.data.model.Category

interface CategoriesAdapterPresenter{

    fun onCategoryClicked(category: Category)
    fun onCategoryLongClick()
    fun showMessage(message: String)
}