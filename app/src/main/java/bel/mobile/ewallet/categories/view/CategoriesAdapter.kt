//package bel.mobile.ewallet.categories.view
//
//import android.util.SparseArray
//import android.util.SparseIntArray
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import bel.mobile.data.model.Category
//import bel.mobile.ewallet.R
//import bel.mobile.ewallet.categories.presentation.CategoriesAdapterPresenter
//import javax.inject.Inject
//
//class CategoriesAdapter @Inject constructor(val presenter: CategoriesAdapterPresenter):
//        RecyclerView.Adapter<RecyclerView.ViewHolder>(), ParentCallback {
//
//    companion object TYPE {
//        const val PARENT = 1
//        const val CHILD = 2
//    }
//
//    var entities: MutableList<Category> = ArrayList()
//    var children: MutableList<Category> = ArrayList()
//
//    lateinit var viewTypes: SparseArray<ViewType>
//    lateinit var headerExpandTracker: SparseIntArray
//
//    fun setList(list: MutableList<Category>){
//        list.forEach { it.subCategories.forEach{ entities.add(it) } }
//        viewTypes = SparseArray(entities.size)
//        headerExpandTracker = SparseIntArray(list.size)
//        notifyDataSetChanged()
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
//        return when(viewType){
//            PARENT -> { ParentViewHolder(view, this) }
//            CHILD -> { ChildViewHolder(view) }
//            else -> { ParentViewHolder(view, this) }
//        }
//    }
//
//    override fun getItemViewType(position: Int): Int =
//            if(entities[position].parentId == 0L) PARENT
//            else CHILD
//
//    override fun getItemCount(): Int{
//
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        when(holder){
//            is ParentViewHolder -> holder.bind(this.entities[position])
//            is ChildViewHolder -> holder.bind(this.entities[position])
//        }
//    }
//
////    fun updateList(newList: MutableList<Category>){
////        val callback = CategoryDiffUtil(entities, newList)
////        val result = DiffUtil.calculateDiff(callback)
////        entities.clear()
////        entities.addAll(newList)
////        result.dispatchUpdatesTo(this)
////    }
//
//
//
//    override fun onClick(position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun isExpanded(position: Int): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    inner class ParentViewHolder(itemView: View,
//                                 private val callback: ParentCallback): RecyclerView.ViewHolder(itemView){
//
//        fun bind(item: Category) {
//            with(itemView){
//
//                setOnClickListener{
//                    callback.onClick(adapterPosition)
//
//                    if(callback.isExpanded(adapterPosition)){
//                        Timber.v("expand")
//                        headerExpandTracker[]
//                    }
//                    else{
//                        Timber.v("collapse")
//                    }
//
//                }
//
//                categoryTitle.text = item.title
//            }
//        }
//    }
//
//    inner class ChildViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
//
//        fun bind(item: Category) {
//            with(itemView){
//                categoryTitle.text = String.format("%d ${item.title}", 2)
//            }
//        }
//
//    }
//
//    inner class ViewType(val dataIndex: Int, val type: Int)
//
//}