package bel.mobile.ewallet.categories.di

import bel.mobile.data.db.interactor.categories.GetCategoryListByParent
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.data.source.repository.base.CategoryDataSource
import bel.mobile.ewallet.categories.presentation.CategoriesAdapterPresenter
import bel.mobile.ewallet.categories.presentation.CategoriesAdapterPresenterImpl
import bel.mobile.ewallet.categories.presentation.CategoryListPresenter
import bel.mobile.ewallet.categories.presentation.CategoryListPresenterImpl
import bel.mobile.ewallet.categories.view.CategoryListView
import bel.mobile.ewallet.categories.view.CategoriesExpandableAdapter
import bel.mobile.ewallet.categories.view.CategoryListFragment
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class CategoryListModule{

    @Binds
    abstract fun provideCategoryListView(categoryListFragment: CategoryListFragment): CategoryListView

    @Module
    companion object {

//        @JvmStatic
//        @Provides
//        fun provideGetCategoriesUseCase(categoryRepository: CategoryDataSource,
//                                        threadExecutor: ThreadExecutor,
//                                        postExecutionThread: PostExecutionThread) =
//                GetCategoryList(categoryRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun providesGetCategoryListByParent(categoryRepository: CategoryDataSource,
                                            threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread) =
                GetCategoryListByParent(categoryRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun provideCategoryListPresenter(view: CategoryListView,
                                         getCategoryList: GetCategoryListByParent): CategoryListPresenter =
                CategoryListPresenterImpl(view, getCategoryList)

        @JvmStatic
        @Provides
        fun provideCategoriesAdapter(presenter: CategoriesAdapterPresenter): CategoriesExpandableAdapter =
                CategoriesExpandableAdapter(presenter)

        @JvmStatic
        @Provides
        fun provideCategoriesAdapterPresenter(view: CategoryListView): CategoriesAdapterPresenter =
                CategoriesAdapterPresenterImpl(view)

    }

}