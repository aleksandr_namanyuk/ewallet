package bel.mobile.ewallet.categories.view

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import bel.mobile.data.model.Category
import bel.mobile.ewallet.R
import bel.mobile.ewallet.categories.presentation.CategoryListPresenter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_categories.*
import javax.inject.Inject

class CategoryListFragment: DaggerFragment(), CategoryListView {

    @Inject lateinit var categoriesAdapter: CategoriesExpandableAdapter
    @Inject lateinit var presenter: CategoryListPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_categories, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind()
        presenter.loadCategories()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

    override fun initAdapter() {
        categories.adapter = categoriesAdapter
        categories.layoutManager = LinearLayoutManager(context)
        categories.setHasFixedSize(true)
        categories.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
    }

    override fun showTitle() {
        activity?.title = getString(R.string.categoriesTitle)
    }

    override fun showEntities(entities: MutableList<Category>) {
        categoriesAdapter.entities = entities
        categoriesAdapter.notifyDataSetChanged()
    }

    override fun showToast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}