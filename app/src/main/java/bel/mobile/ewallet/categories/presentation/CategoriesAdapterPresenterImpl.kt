package bel.mobile.ewallet.categories.presentation

import bel.mobile.data.model.Category
import bel.mobile.ewallet.categories.presentation.CategoriesAdapterPresenter
import bel.mobile.ewallet.categories.view.CategoryListView
import javax.inject.Inject

class CategoriesAdapterPresenterImpl @Inject constructor(private val view: CategoryListView): CategoriesAdapterPresenter {

    override fun showMessage(message: String) {
        view.showToast(message)
    }

    override fun onCategoryClicked(category: Category) {
    }

    override fun onCategoryLongClick() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}