package bel.mobile.ewallet.categories.presentation

import bel.mobile.ewallet.base.BasePresenter

/**
 * Presenter for account list fragment. Contains list of functionality in the fragment,
 * that should be handled before/after actions in the UI.
 */
interface CategoryListPresenter: BasePresenter {

    fun loadCategories()
    fun onCreateSubcategoryClicked()
}