package bel.mobile.ewallet.categories.presentation

import bel.mobile.data.db.interactor.categories.GetCategoryListByParent
import bel.mobile.data.model.Category
import bel.mobile.ewallet.categories.view.CategoryListView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class CategoryListPresenterImpl @Inject constructor(private val view: CategoryListView,
                                                    private val getCategoryList: GetCategoryListByParent): CategoryListPresenter {

    override fun bind() {
        view.initAdapter()
        view.showTitle()
    }

    override fun unBind() {
        getCategoryList.dispose()
    }

    override fun loadCategories() {
        getCategoryList.execute(0, GetCategoryListByParentObserver())
    }

    override fun onCreateSubcategoryClicked() {
    }

    inner class GetCategoryListByParentObserver: DisposableSingleObserver<MutableList<Category>>(){

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }

        override fun onSuccess(t: MutableList<Category>) {
            view.showEntities(t)
        }
    }
}