package bel.mobile.ewallet.categories.view

interface ParentCallback{

    fun onClick(position: Int)
    fun isExpanded(position: Int): Boolean
}