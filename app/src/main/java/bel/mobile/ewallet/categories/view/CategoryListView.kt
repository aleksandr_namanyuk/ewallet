package bel.mobile.ewallet.categories.view

import bel.mobile.data.model.Category
import bel.mobile.ewallet.base.BaseDbListView
import bel.mobile.ewallet.base.HasTitle

interface CategoryListView : BaseDbListView<Category>, HasTitle{

    fun showToast(message: String)
}