package bel.mobile.ewallet.categories.view

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bel.mobile.data.model.Category
import bel.mobile.ewallet.R
import bel.mobile.ewallet.base.BaseAdapter
import bel.mobile.ewallet.categories.presentation.CategoriesAdapterPresenter
import bel.mobile.ewallet.categories.presentation.CategoryDiffUtil
import javax.inject.Inject
import kotlinx.android.synthetic.main.item_category.view.*

class CategoriesExpandableAdapter @Inject constructor(val presenter: CategoriesAdapterPresenter):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object TYPE {
        const val PARENT = 1
        const val CHILD = 2
    }

    var entities: MutableList<Category> = ArrayList()
//        set(value) {
//            entities.clear()
//            entities.addAll(value)
//            notifyDataSetChanged()
//        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)

        return when(viewType){
            PARENT -> { ParentViewHolder(view) }
            CHILD -> { ChildViewHolder(view) }
            else -> { ParentViewHolder(view) }
        }
    }

    override fun getItemViewType(position: Int): Int =
            if(entities[position].parentId == 0L) PARENT
            else CHILD

    override fun getItemCount(): Int = this.entities.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is ParentViewHolder -> holder.bind(this.entities[position])
            is ChildViewHolder -> holder.bind(this.entities[position])
        }
//        holder.bind(this.entities[position])
    }

    fun updateList(newList: MutableList<Category>){
        val callback = CategoryDiffUtil(entities, newList)
        val result = DiffUtil.calculateDiff(callback)
        entities.clear()
        entities.addAll(newList)
        result.dispatchUpdatesTo(this)
    }

    inner class ParentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(item: Category) {
            with(itemView){

                setOnClickListener{
                    if(item.subCategories.size > 0){
                        val newList = entities
                        newList.addAll(adapterPosition+1, item.subCategories.toMutableList())
                        updateList(newList)
                    }
                    presenter.onCategoryClicked(item)
//                    mOnCategorySelectedListener.onCategorySelected(category.getType(), category.getId())
                }

                categoryTitle.text = item.title
//                .setImageDrawable(ResourcesCompat.getDrawable(mContext.getResources(),
//                        getIcon(category.getId().intValue()), null))
            }
        }
    }

    inner class ChildViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(item: Category) {
            with(itemView){
                categoryTitle.text = String.format("%d ${item.title}", 2)
            }
        }

    }

}