package bel.mobile.ewallet.navigation

import android.content.Intent
import android.support.v4.app.Fragment
import bel.mobile.ewallet.account.view.AccountActivity
import bel.mobile.ewallet.account.view.AccountActivity.EXTRA_ACCOUNT_ID

fun Fragment.closeActivity(){
    activity?.finish()
}

fun Fragment.navigateToCreateAccountScreen() {
    startActivity(Intent(activity, AccountActivity::class.java))
}

fun Fragment.navigateToEditAccountScreen(accountId: Long){
    val intent = Intent(activity, AccountActivity::class.java)
    intent.putExtra(EXTRA_ACCOUNT_ID, accountId)
    startActivity(intent)
}