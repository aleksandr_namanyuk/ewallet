package bel.mobile.ewallet.record.presentation

import bel.mobile.ewallet.record.view.RecordView
import javax.inject.Inject

class RecordPresenterImpl @Inject constructor(private val view: RecordView): RecordPresenter {

    override fun bind() {
        view.showTitle()
    }

    override fun unBind() {
    }

}