package bel.mobile.ewallet.record.view

import android.os.Bundle
import android.view.*
import bel.mobile.ewallet.R
import bel.mobile.ewallet.record.presentation.RecordPresenter
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class RecordFragment: DaggerFragment(), RecordView {

    @Inject lateinit var presenter: RecordPresenter

    fun newInstance(): RecordFragment{
        return RecordFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_record, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bind()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.record, menu)
        menu?.setGroupVisible(R.id.recordGroup, true)
        menu?.findItem(R.id.delete)?.isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when(item?.itemId){
        R.id.save -> {
            true
        }
        R.id.delete -> {
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

    override fun showTitle() {
        activity?.setTitle(R.string.title_records)
    }

}
