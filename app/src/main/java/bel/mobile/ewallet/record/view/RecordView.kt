package bel.mobile.ewallet.record.view

import bel.mobile.ewallet.base.BaseView
import bel.mobile.ewallet.base.HasTitle

interface RecordView: BaseView, HasTitle