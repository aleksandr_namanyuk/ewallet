package bel.mobile.ewallet.record.di

import bel.mobile.ewallet.record.view.RecordFragment
import bel.mobile.ewallet.record.view.RecordView
import bel.mobile.ewallet.record.presentation.RecordPresenter
import bel.mobile.ewallet.record.presentation.RecordPresenterImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RecordFragmentModel{

    @Binds
    abstract fun provideRecordFragmentView(fragment: RecordFragment): RecordView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideRecordPresenter(view: RecordView): RecordPresenter =
                RecordPresenterImpl(view)

    }
}