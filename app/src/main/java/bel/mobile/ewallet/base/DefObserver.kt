package bel.mobile.ewallet.base

import io.reactivex.observers.DisposableObserver

/**
 * Default Disposable Observer for the type.
 * This is Rx callback.
 */
open class DefObserver<T> : DisposableObserver<T>() {

    override fun onNext(t: T) {
        // no-op by default.
    }

    override fun onComplete() {
        // no-op by default.
    }

    override fun onError(exception: Throwable) {
        exception.printStackTrace()
    }
}