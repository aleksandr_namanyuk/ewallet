package bel.mobile.ewallet.base

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

/**
 * Base Adapter for the list of <T> to represent on the screen
 */
abstract class BaseAdapter<T>: RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder>(){

    var entities: MutableList<T> = ArrayList()
        set(value) {
            entities.clear()
            entities.addAll(value)
            notifyDataSetChanged()
        }

    override fun getItemCount(): Int = this.entities.size

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(this.entities[position])
    }

    fun getEntity(entityId: Int): T = entities[entityId]

    abstract inner class BaseViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        /**
         * Should implement the binding of the UI components in the item_xxx.xml from the item parameter <T>
         */
        abstract fun bind(item: T)
    }
}