package bel.mobile.ewallet.base

/**
 * Interface for Activities to implement button back pressed functionality.
 */
interface HasParentActivity {

    fun closeParentActivity()
}