package bel.mobile.ewallet.base

interface BasePresenter{

    fun bind()
    fun unBind()
}