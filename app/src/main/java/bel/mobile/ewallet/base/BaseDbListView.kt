package bel.mobile.ewallet.base

interface BaseDbListView<T>{

    fun initAdapter()
    fun showEntities(entities: MutableList<T>)
}