package bel.mobile.ewallet.account.di

import bel.mobile.ewallet.account.view.AccountFragment
import bel.mobile.ewallet.settings.di.SettingsModule
import bel.mobile.ewallet.settings.view.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AccountModule{

    @ContributesAndroidInjector(modules = [AccountActivityModule::class])
    abstract fun provideAccountFragment(): AccountFragment

    @ContributesAndroidInjector(modules = [SettingsModule::class])
    abstract fun provideSettingsFragment(): SettingsFragment

}