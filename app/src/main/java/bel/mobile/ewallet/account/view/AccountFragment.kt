package bel.mobile.ewallet.account.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import bel.mobile.data.db.AccountTable
import bel.mobile.data.model.Account
import bel.mobile.ewallet.R
import bel.mobile.ewallet.account.presentation.AccountPresenter
import bel.mobile.ewallet.account.view.AccountActivity.EXTRA_ACCOUNT_ID
import bel.mobile.ewallet.navigation.closeActivity
import bel.mobile.ewallet.navigation.navigateToEditAccountScreen
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_account.*
import javax.inject.Inject

/**
 * Fragment for handling Account Entity.
 */
class AccountFragment: DaggerFragment(), AccountFragmentView {

    @Inject lateinit var presenter: AccountPresenter

    companion object {
        val TAG = "AccountFragment"
        fun newInstance(accountId: Long) = AccountFragment().apply{
            val args = Bundle()
            args.putLong(EXTRA_ACCOUNT_ID, accountId)
            arguments = args
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?{
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.loadAccount(getAccountIdArg())
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unBind()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater) {
        inflater.inflate(R.menu.account, menu)
        menu?.setGroupVisible(R.id.accountGroup, true)
        menu?.findItem(R.id.delete)?.isVisible = isEdit()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when(item?.itemId){
        R.id.save -> {
            this.onSaveOptionSelected()
            true
        }
        R.id.delete -> {
            presenter.onDeleteOptionSelected(getAccountIdArg())
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun onSaveOptionSelected(){
        presenter.onSaveOptionSelected(createAccountModel())
    }

    private fun createAccountModel(): Account {
        val account = Account()
        account.id = getAccountIdArg()
        account.name = inputEditAccountName.text.toString()
        account.balance = validateBalance()
        return account
    }

    private fun validateBalance() =
            etBalance.text.toString().toDoubleOrNull() ?: AccountTable.defaultBalance

//    TODO: clean code
    override fun isEdit(): Boolean =
        getAccountIdArg() > 0

//    TODO: clean code
    private fun getAccountIdArg() =
        arguments!!.getLong(EXTRA_ACCOUNT_ID, 0)

    override fun displayAccount(account: Account){
        inputEditAccountName.setText(account.name)
        etBalance.setText(account.balance.toString())
    }

    override fun displayTitleEmptyError() {
        inputLayoutAccountName.isErrorEnabled = true
        inputEditAccountName.error = getString(R.string.errorEmptyAccountName)
    }

    override fun hideTitleError() {
        inputLayoutAccountName.isErrorEnabled = false
        inputEditAccountName.error = null
    }

    override fun closeParentActivity() {
        this.closeActivity()
        this.navigateToEditAccountScreen(1)
    }

    override fun displayToast(message: String) {
        val toast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.BOTTOM, 0, 15)
        toast.show()
    }
}