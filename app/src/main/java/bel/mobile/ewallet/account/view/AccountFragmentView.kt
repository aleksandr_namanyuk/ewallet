package bel.mobile.ewallet.account.view

import bel.mobile.data.model.Account
import bel.mobile.ewallet.base.BaseView
import bel.mobile.ewallet.base.HasParentActivity

interface AccountFragmentView: HasParentActivity, BaseView {

    fun displayAccount(account: Account)
    fun displayTitleEmptyError()
    fun hideTitleError()
    fun displayToast(message: String)
    fun isEdit(): Boolean
}