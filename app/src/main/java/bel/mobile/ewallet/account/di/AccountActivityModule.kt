package bel.mobile.ewallet.account.di

import bel.mobile.data.source.repository.base.AccountDataSource
import bel.mobile.data.db.interactor.account.DeleteAccount
import bel.mobile.data.db.interactor.account.GetAccount
import bel.mobile.data.db.interactor.account.PutAccount
import bel.mobile.data.executor.PostExecutionThread
import bel.mobile.data.executor.ThreadExecutor
import bel.mobile.ewallet.account.presentation.AccountPresenter
import bel.mobile.ewallet.account.presentation.AccountPresenterImpl
import bel.mobile.ewallet.account.view.AccountFragment
import bel.mobile.ewallet.account.view.AccountFragmentView
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AccountActivityModule{

    @Binds
    abstract fun provideAccountFragmentView(accountFragment: AccountFragment): AccountFragmentView

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun provideGetAccountUseCase(accountRepository: AccountDataSource,
                                     threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread) =
                GetAccount(accountRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun providePutAccountUseCase(accountRepository: AccountDataSource,
                                     threadExecutor: ThreadExecutor,
                                     postExecutionThread: PostExecutionThread) =
                PutAccount(accountRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun provideDeleteAccountUseCase(accountRepository: AccountDataSource,
                                        threadExecutor: ThreadExecutor,
                                        postExecutionThread: PostExecutionThread) =
                DeleteAccount(accountRepository, threadExecutor, postExecutionThread)

        @JvmStatic
        @Provides
        fun provideAccountPresenter(view: AccountFragmentView,
                                    getAccount: GetAccount,
                                    putAccount: PutAccount,
                                    deleteAccount: DeleteAccount): AccountPresenter =
                AccountPresenterImpl(view, getAccount, putAccount, deleteAccount)
    }
}