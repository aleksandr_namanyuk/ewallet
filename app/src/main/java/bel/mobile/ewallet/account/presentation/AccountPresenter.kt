package bel.mobile.ewallet.account.presentation

import bel.mobile.data.model.Account
import bel.mobile.ewallet.base.BasePresenter

interface AccountPresenter: BasePresenter {

    fun loadAccount(accountId: Long)
    fun onSaveOptionSelected(accountModel: Account)
    fun onDeleteOptionSelected(accountId: Long): Boolean?
}