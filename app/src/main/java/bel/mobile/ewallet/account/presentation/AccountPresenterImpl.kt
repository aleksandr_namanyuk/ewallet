package bel.mobile.ewallet.account.presentation

import android.support.v4.app.Fragment
import bel.mobile.data.db.interactor.account.DeleteAccount
import bel.mobile.data.db.interactor.account.GetAccount
import bel.mobile.data.db.interactor.account.PutAccount
import bel.mobile.data.model.Account
import bel.mobile.ewallet.account.view.AccountFragmentView
import bel.mobile.ewallet.base.DefObserver
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

/**
 * Presenter Implementation for handling account.
 * If account does not exist -> create account.
 * If account exists -> edit an account.
 */
class AccountPresenterImpl @Inject constructor(val view: AccountFragmentView,
//                                               private val model: Account,
                                               private val getAccount: GetAccount,
                                               private val putAccount: PutAccount,
                                               private val deleteAccount: DeleteAccount): AccountPresenter {

    override fun bind(){
//        this.loadAccount()
    }

    override fun loadAccount(accountId: Long){
        getAccount.execute(accountId, GetAccountObserver())
    }

    override fun unBind(){
        getAccount.dispose()
        putAccount.dispose()
        deleteAccount.dispose()
    }

    override fun onSaveOptionSelected(accountModel: Account) {
        if(accountModel.name.isEmpty()){
            view.displayTitleEmptyError()
            return@onSaveOptionSelected
        }

        putAccount.execute(accountModel, CreateAccountObserver())
    }

    override fun onDeleteOptionSelected(accountId: Long): Boolean? {
        deleteAccount.execute(accountId, DeleteAccountObserver())
        return true
    }

    inner class GetAccountObserver: DefObserver<Account>() {

        override fun onNext(t: Account) {
            view.displayAccount(t)
        }

        override fun onError(exception: Throwable) {
            exception.printStackTrace()
        }
    }

    inner class CreateAccountObserver: DisposableObserver<Long>(){

        override fun onNext(t: Long) {
            view.displayToast("Account has been created with id #$t")
        }

        override fun onError(exception: Throwable) {
            exception.printStackTrace()
        }

        override fun onComplete() {
            view.closeParentActivity()
        }
    }

    inner class DeleteAccountObserver: DisposableCompletableObserver(){

        override fun onComplete() {
            view.closeParentActivity()
            view.displayToast("Account has been deleted")
        }

        override fun onError(e: Throwable) {
            e.printStackTrace()
        }
    }

}