package bel.mobile.ewallet.account.view;

import android.os.Bundle;
import android.support.annotation.Nullable;

import bel.mobile.ewallet.R;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * Activity to handle Account entity.
 */
public class AccountActivity extends DaggerAppCompatActivity {

    public static final String EXTRA_ACCOUNT_ID = "extra_account_id";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.accountFrame,
                        AccountFragment.Companion.newInstance(
                                getIntent().getLongExtra(EXTRA_ACCOUNT_ID, 0L)))
                .addToBackStack(null)
                .commit();
    }

}
