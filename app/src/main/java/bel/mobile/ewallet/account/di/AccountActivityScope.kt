package bel.mobile.ewallet.account.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AccountActivityScope