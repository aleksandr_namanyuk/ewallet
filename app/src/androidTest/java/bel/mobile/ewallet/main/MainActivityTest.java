package bel.mobile.ewallet.main;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import bel.mobile.ewallet.R;
import bel.mobile.ewallet.main.view.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testMainActivityTitle() {
        String title = activityTestRule.getActivity().getTitle().toString();
        assertEquals("eWallet", title);
    }

    @Test
    public void testActivityLayout(){
//        Robolectric.
    }

    @Test
    public void testChangingToAccountFragment(){
        onView(withId(R.id.navigation_categories)).perform(click());

        String title = activityTestRule.getActivity().getTitle().toString();
        assertEquals(getResourceString(R.string.title_categories), title);
    }

    @Test
    public void testChangingToSettingsFragment() {
        onView(withId(R.id.navigation_settings)).perform(click());

        String title = activityTestRule.getActivity().getTitle().toString();
        assertEquals(getResourceString(R.string.title_settings), title);
    }

    private String getResourceString(int id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        return targetContext.getResources().getString(id);
    }
}
