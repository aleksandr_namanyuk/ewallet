package bel.mobile.ewallet.di

import bel.mobile.data.source.repository.base.AccountDataSource
import bel.mobile.data.source.repository.base.CategoryDataSource
import dagger.Module
import dagger.Provides
import com.nhaarman.mockito_kotlin.mock
import javax.inject.Singleton

@Module
class TestDataModule {

//    @Provides
//    @Singleton
//    fun provideAccountDataMapper(): AccountDataMapper =
//            mock()

//    @Provides
//    @Singleton
//    fun provideAccountModelDataMapper(): AccountModelDataMapper =
//            mock()

//    @Provides
//    @Singleton
//    fun provideCategoryDataMapper(): CategoryDataMapper =
//            mock()

//    @Provides
//    @Singleton
//    fun provideCategoryModelDataMapper(): CategoryModelDataMapper =
//            mock()

    @Provides
    @Singleton
    fun provideAccountRepository(): AccountDataSource =
            mock()

    @Provides
    @Singleton
    fun provideCategoryRepository(): CategoryDataSource =
            mock()

}