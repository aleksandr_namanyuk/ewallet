package bel.mobile.ewallet.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    TestAppModule::class,
    TestDataModule::class,
    TestDatabaseModule::class,
    ActivityBindingModule::class])
interface TestAppComponent : AppComponent {

    @Component.Builder
    interface Builder {
        /**
         * Allows the instance of Application to be injected in the AppComponent component
         * and be used in AppModule without injecting it to the constructor.
         */
        @BindsInstance
        fun application(application: Application): TestAppComponent.Builder

        fun build(): TestAppComponent
    }

    fun inject(application: TestApplication)
}