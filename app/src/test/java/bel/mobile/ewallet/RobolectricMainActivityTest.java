package bel.mobile.ewallet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import bel.mobile.ewallet.accounts.view.AccountListFragment;
import bel.mobile.ewallet.main.view.MainActivity;

import static org.junit.Assert.assertNotNull;
import static org.robolectric.shadows.support.v4.SupportFragmentTestUtil.startFragment;

@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class RobolectricMainActivityTest {

    @Test
    public void testFragment() {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);
//        activity.findViewById(R.id.navigation);

        AccountListFragment fragment = AccountListFragment.newInstance();
        startFragment( fragment );
        assertNotNull( fragment );

//        assertThat();
//        assertThat(results.getText().toString()).isEqualTo("Robolectric Rocks!");
    }


}
