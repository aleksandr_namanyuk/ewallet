package bel.mobile.ewallet.di

import bel.mobile.data.source.local.AccountDao
import bel.mobile.data.source.local.CategoryDao
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestDatabaseModule{

    @Provides
    @Singleton
    fun provideAccountDao(): AccountDao =
            mock()

    @Provides
    @Singleton
    fun provideCategoryDao(): CategoryDao =
            mock()
}