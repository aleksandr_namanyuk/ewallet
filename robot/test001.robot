*** Settings ***
Library  AppiumLibrary  10
Suite Setup  Open Application  http://localhost:4723/wd/hub
             ...  platformName=Android
             ...  deviceName=emulator-5554
             ...  udid=3e2b9aa541b53a71
             ...  app=/Users/lunni/AndroidStudioProjects/eWallet/app/build/outputs/apk/debug/app-debug.apk
             ...  automationName=uiautomator2
Suite Teardown  Close Application

*** Test Cases ***
App Presents the Login Page When Launches
    Page Should Contain Element  id=btn_login
