package bel.mobile.domain.interactor;

import bel.mobile.domain.model.AccountModel;
import bel.mobile.domain.model.CategoryModel;

public class DomainTestData {

    /* Start of Account */

    public static final Long TEST_ACCOUNT_ID = 15L;
    private static final String TEST_ACCOUNT_NAME = "Debit";
    private static final Double TEST_ACCOUNT_BALANCE = 150.4;
    public static final Integer TEST_ACCOUNT_CURRENCY_ID = 1;

    public static final AccountModel TEST_NEW_ACCOUNT = new AccountModel(TEST_ACCOUNT_ID, TEST_ACCOUNT_NAME,
            TEST_ACCOUNT_BALANCE, TEST_ACCOUNT_CURRENCY_ID);

    /* End of Account */

    /* Start of Category */

    public static final Long TEST_CATEGORY_ID = 5L;
    private static final String TEST_CATEGORY_TITLE = "Shopping";

    public static final CategoryModel TEST_CATEGORY_PARENT = new CategoryModel(2L, "Shopping", 1);

    public static final CategoryModel TEST_NEW_CATEGORY = new CategoryModel(TEST_CATEGORY_ID, TEST_CATEGORY_TITLE, 1);

    /* End of Category */
}
