package bel.mobile.domain.interactor.base;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import bel.mobile.domain.interactor.base.core.UseCase;
import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

import static com.google.common.truth.Truth.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UseCaseTest {

    private UseCaseTestClass useCase;
    private TestDisposableObserver<Object> testObserver;

    @Before
    public void setUp(){
        useCase = new UseCaseTestClass();
        testObserver = new TestDisposableObserver<>();
    }

    @Test
    public void testSubscriptionWhenExecutingUseCase() {
        useCase.execute(testObserver);
        useCase.dispose();

        assertThat(testObserver.isDisposed()).isTrue();
    }


    private static class UseCaseTestClass extends UseCase {

        public UseCaseTestClass() {
            super();
        }

        public void execute(DisposableObserver<Object> observer){
            getCompositeDisposable().add(Observable.just(1).subscribeWith(observer));
        }
    }

    private static class TestDisposableObserver<T> extends DisposableObserver<T> {

        @Override public void onNext(T value) {
        }

        @Override public void onError(Throwable e) {
            // no-op by default.
        }

        @Override public void onComplete() {
            // no-op by default.
        }
    }

}
