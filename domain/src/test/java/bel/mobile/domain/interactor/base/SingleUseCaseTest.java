package bel.mobile.domain.interactor.base;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.base.core.SingleUseCase;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)
public class SingleUseCaseTest {

    private SingleTestClass singleUseCase;
    private TestObserver<Object> testObserver;

    @Rule public ExpectedException expectedException = ExpectedException.none();

    @Mock
    ThreadExecutor mockThreadExecutor;
    @Mock
    PostExecutionThread mockPostExecutionThread;

    @Before
    public void setUp() {
        singleUseCase = new SingleTestClass(mockThreadExecutor, mockPostExecutionThread);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldComplete(){
        singleUseCase.execute().subscribe(testObserver);
        testObserver.assertComplete();
    }

    public static class SingleTestClass extends SingleUseCase<Object>{

        SingleTestClass(@NotNull ThreadExecutor threadExecutor, @NotNull PostExecutionThread postExecutionThread) {
            super(threadExecutor, postExecutionThread);
        }

        @NotNull
        @Override
        public Single<Object> createSingle() {
            return Single.just(new Object());
        }
    }
}
