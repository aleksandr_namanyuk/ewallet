package bel.mobile.domain.interactor.account;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.DomainTestData;
import bel.mobile.domain.model.AccountModel;
import bel.mobile.domain.repository.AccountRepository;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class PutAccountTest {

    private TestObserver<Long> testObserver;
    private PutAccount putAccount;

    @Mock private AccountRepository mockAccountRepository;
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;

    @Before
    public void setUp(){
        putAccount = new PutAccount(mockAccountRepository,
                mockThreadExecutor, mockPostExecutionThread);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldPutAccountAndReturnId(){
        final AccountModel account = DomainTestData.TEST_NEW_ACCOUNT;

        when(mockAccountRepository.put(account))
                .thenReturn(Single.just(DomainTestData.TEST_ACCOUNT_ID));

        putAccount.execute(account).subscribe(testObserver);

        testObserver.assertValue(DomainTestData.TEST_ACCOUNT_ID);
        testObserver.assertSubscribed();
        testObserver.assertComplete();
    }

}
