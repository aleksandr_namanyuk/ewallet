package bel.mobile.domain.interactor.account;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.DomainTestData;
import bel.mobile.domain.model.AccountModel;
import bel.mobile.domain.repository.AccountRepository;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class GetAccountTest {

    private TestObserver<AccountModel> testSubscriber;
    private GetAccount getAccount;

    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private AccountRepository mockAccountRepository;

    @Before
    public void setUp(){
        testSubscriber = new TestObserver<>();
        getAccount = new GetAccount(mockAccountRepository,
                mockThreadExecutor, mockPostExecutionThread);
    }

    @Test
    public void happyCaseTest(){
        getAccount.createObservable(DomainTestData.TEST_ACCOUNT_CURRENCY_ID);

        verify(mockAccountRepository).item(DomainTestData.TEST_ACCOUNT_CURRENCY_ID);
        Mockito.verifyNoMoreInteractions(mockAccountRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test
    public void shouldReturnCorrectAccount() throws Exception {

        when(mockAccountRepository.item(DomainTestData.TEST_ACCOUNT_ID))
                .thenReturn(Observable.just(DomainTestData.TEST_NEW_ACCOUNT));

        getAccount.execute(DomainTestData.TEST_ACCOUNT_ID).subscribe(testSubscriber);

        verify(mockAccountRepository, times(1)).item(DomainTestData.TEST_ACCOUNT_ID);
        Mockito.verifyNoMoreInteractions(mockAccountRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);

        testSubscriber.assertComplete();
        testSubscriber.assertValue(DomainTestData.TEST_NEW_ACCOUNT);
    }

    @Test
    public void shouldReturnNothingAndCompleteIfThereIsNoSearchedAccount() throws Exception {
        when(mockAccountRepository.item(DomainTestData.TEST_ACCOUNT_ID)).thenReturn(Observable.<AccountModel>empty());

        testSubscriber = getAccount.execute(DomainTestData.TEST_ACCOUNT_ID).test();

        verify(mockAccountRepository, times(1)).item(DomainTestData.TEST_ACCOUNT_ID);
        Mockito.verifyNoMoreInteractions(mockAccountRepository);

        testSubscriber.assertSubscribed();
        testSubscriber.assertNoErrors();
        testSubscriber.assertNoValues();
        testSubscriber.assertComplete();
        testSubscriber.assertValueCount(0);
    }


}
