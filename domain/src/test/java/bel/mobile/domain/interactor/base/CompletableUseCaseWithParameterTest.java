package bel.mobile.domain.interactor.base;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.TimeUnit;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.base.core.CompletableUseCaseWithParameter;
import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;

import static com.google.common.truth.Truth.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CompletableUseCaseWithParameterTest {

    private CompletableUseCaseWithParameterTestClass completableUseCase;
    private TestObserver<Long> testObserver;
    private TestScheduler testScheduler;

    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;

    @Rule public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp(){
        completableUseCase = new CompletableUseCaseWithParameterTestClass(mockThreadExecutor, mockPostExecutionThread);
        testObserver = new TestObserver<>();
        testScheduler = new TestScheduler();

//        given(mockPostExecutionThread.getScheduler()).willReturn(testScheduler);
    }

    @Test
    public void shouldComplete(){
        completableUseCase.execute(5L).subscribe(testObserver);

        assertThat(completableUseCase.i).isAtLeast(1);
        testScheduler.advanceTimeBy( 10, TimeUnit.SECONDS);

        testObserver.assertComplete();
    }

    @Test
    public void testShouldFailWhenExecuteWithNullObserver() {
        expectedException.expect(IllegalArgumentException.class);
        completableUseCase.execute( 5L,null);
    }

    public static class CompletableUseCaseWithParameterTestClass extends CompletableUseCaseWithParameter<Long>{

        private int i = 0;

        public CompletableUseCaseWithParameterTestClass(@NotNull ThreadExecutor threadExecutor,
                                                        @NotNull PostExecutionThread postExecutionThread) {
            super(threadExecutor, postExecutionThread);
        }

        @NotNull
        @Override
        public Completable createCompletable(Long o) {
            i++;
            return Completable.complete();
        }
    }

}
