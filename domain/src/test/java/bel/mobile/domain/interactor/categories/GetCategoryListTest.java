package bel.mobile.domain.interactor.categories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.DomainTestData;
import bel.mobile.domain.model.CategoryModel;
import bel.mobile.domain.repository.CategoryRepository;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class GetCategoryListTest {

    private TestObserver<List<CategoryModel>> testSubscriber;
    private GetCategoryList getCategoryList;

    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private CategoryRepository mockCategoryRepository;

    @Before
    public void setUp() throws Exception {
        testSubscriber = new TestObserver<>();
        getCategoryList = new GetCategoryList(mockCategoryRepository,
                mockThreadExecutor, mockPostExecutionThread);
    }

    @Test
    public void happyCaseTest(){
        getCategoryList.createSingle();

        verify(mockCategoryRepository).items();
        Mockito.verifyNoMoreInteractions(mockCategoryRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test
    public void shouldReturnCorrectAccountList() throws Exception {
        final List<CategoryModel> categories = new ArrayList<>(2);
        categories.add(DomainTestData.TEST_NEW_CATEGORY);
        categories.add(DomainTestData.TEST_NEW_CATEGORY);

        when(mockCategoryRepository.items()).thenReturn(Single.just(categories));

        getCategoryList.execute().subscribe(testSubscriber);

        verify(mockCategoryRepository, times(1)).items();
        Mockito.verifyNoMoreInteractions(mockCategoryRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);

        testSubscriber.assertComplete();
        testSubscriber.assertValue(categories);
    }

}
