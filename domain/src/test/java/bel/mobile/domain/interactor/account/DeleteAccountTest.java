package bel.mobile.domain.interactor.account;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.DomainTestData;
import bel.mobile.domain.repository.AccountRepository;
import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class DeleteAccountTest {

    private TestObserver<Long> testObserver;
    private DeleteAccount deleteAccount;

    @Mock private AccountRepository mockAccountRepository;
    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;

    @Before
    public void setUp(){
        deleteAccount = new DeleteAccount(mockAccountRepository,
                mockThreadExecutor, mockPostExecutionThread);
        testObserver = new TestObserver<>();
    }

    @Test
    public void shouldPutAccountAndReturnId(){
        when(mockAccountRepository.deleteById(DomainTestData.TEST_ACCOUNT_ID))
                .thenReturn(Completable.complete());

        deleteAccount.execute(DomainTestData.TEST_ACCOUNT_ID).subscribe(testObserver);

        testObserver.assertNoErrors();
        testObserver.assertSubscribed();
        testObserver.assertComplete();
    }
}
