package bel.mobile.domain.interactor.accounts;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import bel.mobile.domain.executors.PostExecutionThread;
import bel.mobile.domain.executors.ThreadExecutor;
import bel.mobile.domain.interactor.DomainTestData;
import bel.mobile.domain.model.AccountModel;
import bel.mobile.domain.repository.AccountRepository;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(org.mockito.junit.MockitoJUnitRunner.class)
public class GetAccountsListTest {

    private TestObserver<List<AccountModel>> testSubscriber;
    private GetAccountsList getAccountsList;

    @Mock private ThreadExecutor mockThreadExecutor;
    @Mock private PostExecutionThread mockPostExecutionThread;
    @Mock private AccountRepository mockAccountRepository;

    @Before
    public void setUp() throws Exception {
        testSubscriber = new TestObserver<>();
        getAccountsList = new GetAccountsList(mockAccountRepository,
                mockThreadExecutor, mockPostExecutionThread);
    }

    @Test
    public void happyCaseTest(){
        getAccountsList.createSingle();

        verify(mockAccountRepository).items();
        Mockito.verifyNoMoreInteractions(mockAccountRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);
    }

    @Test
    public void shouldReturnCorrectAccountList() throws Exception {
        final List<AccountModel> accounts = new ArrayList<>(2);
        accounts.add(DomainTestData.TEST_NEW_ACCOUNT);
        accounts.add(DomainTestData.TEST_NEW_ACCOUNT);

        when(mockAccountRepository.items()).thenReturn(Single.just(accounts));

        getAccountsList.execute().subscribe(testSubscriber);

        verify(mockAccountRepository, times(1)).items();
        Mockito.verifyNoMoreInteractions(mockAccountRepository);
        Mockito.verifyZeroInteractions(mockThreadExecutor);
        Mockito.verifyZeroInteractions(mockPostExecutionThread);

        testSubscriber.assertComplete();
        testSubscriber.assertValue(accounts);
    }

    @Test
    public void shouldReturnEmptyListIfUserHasNoAccounts() throws Exception {
        final List<AccountModel> accounts = new ArrayList<>(0);

        when(mockAccountRepository.items()).thenReturn(Single.just(accounts));

        getAccountsList.execute().subscribe(testSubscriber);

        verify(mockAccountRepository, times(1)).items();
        Mockito.verifyNoMoreInteractions(mockAccountRepository);

        testSubscriber.assertComplete();
        testSubscriber.assertValue(accounts);
    }

}
